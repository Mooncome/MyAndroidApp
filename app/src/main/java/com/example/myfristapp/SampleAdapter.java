package com.example.myfristapp;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.example.myfristapp.Retrofit.DataresItem;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by DELL on 8/30/2016.
 */
public class SampleAdapter extends RecyclerView.Adapter<SampleAdapter.SampleHolder> implements ItemClickListener {
    private final Context context;
    private DataresItem data;
    private ArrayList<SampleImageItem> dataFood;

    public SampleAdapter(ArrayList<SampleImageItem> dataFood,Context context) {

        this.dataFood = dataFood;
        this.context = context;


    }

    @Override
    public SampleAdapter.SampleHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemLayoutView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.res, null);

        final SampleAdapter.SampleHolder viewHolder = new SampleAdapter.SampleHolder(itemLayoutView,this);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(SampleAdapter.SampleHolder holder, int position) {
        SampleImageItem home1 = dataFood.get(position);
//        holder.imageView.setImageResource(home1.getImgeView());
        Picasso.with(context)
                .load(home1.getImgeView())

                .into(holder.imageView);
    }

    @Override
    public int getItemCount() {
        return dataFood.size();
    }

    @Override
    public void onItemClick(View view, int position) {
        if (position==0) {
            //Toast.makeText(ResAdapter.this,"",Toast.LENGTH_SHORT).show();

            context.startActivity(new Intent(context, DataRestaurantActivity1.class));
        }else {
            context. startActivity(new Intent(context, DataRestaurantActivity1.class));


        }

//        Intent intent = new Intent(context, DataRestaurantActivity1.class);
//        Bundle bundle = new Bundle();
//        bundle.putInt("idrestauranFood1", data.getDataRestaurant().get(position).getIdrestauran());
//        bundle.putInt("idtypeFood1", data.getDataRestaurant().get(position).getIdtype());
//        bundle.putString("imgFood1", data.getDataRestaurant().get(position).getImage());
//        bundle.putString("nameFood1", data.getDataRestaurant().get(position).getName());
//        bundle.putString("addFood1",data.getDataRestaurant().get(position).getAdd());
//        bundle.putString("telFood1",data.getDataRestaurant().get(position).getTel());
//        bundle.putInt("minFood1", data.getDataRestaurant().get(position).getPriceMin());
//        bundle.putInt("maxFood1", data.getDataRestaurant().get(position).getPriceMax());
//        bundle.putString("mapFood1", data.getDataRestaurant().get(position).getMap());
//        intent.putExtras(bundle);
//        context.startActivity(intent);

    }

    public class SampleHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        public ImageView imageView;
        public ItemClickListener mitemClickListener;

        public SampleHolder(View itemView,ItemClickListener itemClickListener) {
            super(itemView);
            imageView = (ImageView) itemView.findViewById(R.id.res_layout);
            mitemClickListener = itemClickListener;
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            mitemClickListener.onItemClick(v,getPosition());
        }
    }
}
