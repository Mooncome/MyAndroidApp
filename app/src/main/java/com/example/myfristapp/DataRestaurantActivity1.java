package com.example.myfristapp;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.myfristapp.Activity.MenuActivity;
import com.example.myfristapp.Adapter.DataResAdapter;
import com.example.myfristapp.Retrofit.MenuImageItem;
import com.example.myfristapp.Retrofit.SimpleRetrofit;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.squareup.picasso.Picasso;

import retrofit.Call;
import retrofit.Callback;
import retrofit.GsonConverterFactory;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * Created by DELL on 7/13/2016.
 */
public class DataRestaurantActivity1 extends Activity implements View.OnClickListener,OnMapReadyCallback {

    private RecyclerView recyclerView,recyclerView1;
    private DataResAdapter dataAdapter;
    private Context activity;
    private String img_food;
    private String name_food;
    private ImageView imageView_food;
    private TextView textView_name;
    private MapView mMapView;
    private GoogleMap googleMap;
    private String add_food;
    private String tel_food;
    private int min_food;
    private int max_food;
    private String map_food;
    private String map;
    private int idrestauran;
    private int idtype;
    private String name;
    private String add;
    private String tel;
    private int pricemin;
    private int pricemax;
    private String image;
    private TextView textView_add;
    private TextView textView_tel;
    private TextView textView_min;
    private TextView textView_mix;
    private TextView textView_map;
    private int idmenu;
    private int idrestaurant;
    private String price;
    private String photo;
    private String idrestauran_food;
    private String idtype_food;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.restaurant);

        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        recyclerView1 = (RecyclerView) findViewById(R.id.recycler_view1);

        Intent intent = this.getIntent();
        Bundle extra = intent.getExtras();

        if (extra!=null){
            idrestauran_food = extra.getString("idrestauranFood1");
            idtype_food = extra.getString("idtypeFood1");
            img_food = extra.getString("imgFood1");
            name_food =extra.getString("nameFood1");
            add_food = extra.getString("addFood1");
            tel_food = extra.getString("telFood1");
            min_food = extra.getInt("minFood1");
            max_food = extra.getInt("maxFood1");
            map_food = extra.getString("mapFood1");


        }

        imageView_food = (ImageView) findViewById(R.id.image_res);
        textView_name = (TextView) findViewById(R.id.textView_name1);
        textView_add = (TextView)findViewById(R.id.tv_add);
        textView_tel = (TextView)findViewById(R.id.tv_tel);
        textView_min = (TextView) findViewById(R.id.tv_pricemin);
        textView_mix = (TextView)findViewById(R.id.tv_pricemax);
//        textView_map = (TextView)findViewById(R.id.mapView1);


if (!img_food.equals("")){
        Picasso.with(this)
                .load(img_food)
                .into(imageView_food);
}

        textView_name.setText(name_food);
        textView_add.setText(add_food);
        textView_tel.setText(tel_food);
        textView_min.setText(""+min_food);
        textView_mix.setText(""+max_food);



        Retrofit client = new Retrofit.Builder()
                .baseUrl(ApplicationConfig.URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        SimpleRetrofit simpleRetrofit = client.create(SimpleRetrofit.class);

        Call<MenuImageItem> call = simpleRetrofit.getMenuImageItem();
        call.enqueue(new Callback<MenuImageItem>() {
                         @Override
                         public void onResponse(Response<MenuImageItem> response) {
                             Log.d("", String.valueOf(response.raw()));

                             Log.d("", String.valueOf(response.body()));

                             if (response.body() != null) {
                                 initRecycleGridView(response.body());


                             }
                         }

            private void initRecycleGridView(MenuImageItem body) {
                recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 2));
                DataResAdapter dataAdapter = new DataResAdapter(body, DataRestaurantActivity1.this);
                recyclerView.setAdapter(dataAdapter);




            }

            @Override
            public void onFailure(Throwable t) {
                Log.d("", String.valueOf(t));
            }
        });
        MapFragment mapFragment = (MapFragment) getFragmentManager()
                .findFragmentById(R.id.map);
        mMapView.getMapAsync(this);

        mMapView = (MapView) findViewById(R.id.map);
        mMapView.onCreate(savedInstanceState);

        mMapView.onResume();// needed to get the map to display immediately

        try {
            MapsInitializer.initialize(getActivity().getApplicationContext());
        } catch (Exception e) {
            e.printStackTrace();
        }


        String[] parts = map_food.split(",");
        String partLatitude= parts[0];
        String partLongitude= parts[1];

//        googleMap = mMapView.getMap();
        // latitude and longitude
        double latitude = Double.parseDouble(partLatitude);
        double longitude = Double.parseDouble(partLongitude);

        // create marker
        MarkerOptions marker = new MarkerOptions().position(
                new LatLng(latitude, longitude)).title("");

        // Changing marker icon
        marker.icon(BitmapDescriptorFactory
                .defaultMarker(BitmapDescriptorFactory.HUE_ROSE));

        // adding marker
        googleMap.addMarker(marker);
        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(new LatLng(latitude, longitude)).zoom(15).build();
        googleMap.animateCamera(CameraUpdateFactory
                .newCameraPosition(cameraPosition));

        // Perform any camera updates here
        return ;

    }

    public Context getActivity() {
        return activity;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.bttorder:

                startActivity(new Intent(this, MenuActivity.class));

                break;
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

    }
}
