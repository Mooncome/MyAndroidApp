package com.example.myfristapp.Adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.myfristapp.Activity.BuffetActivity;
import com.example.myfristapp.Activity.DataRestaurantActivity;
import com.example.myfristapp.Item.DataresImageItem;
import com.example.myfristapp.ItemClickListener;
import com.example.myfristapp.R;
import com.example.myfristapp.Retrofit.DataresItem;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by DELL on 7/13/2016.
 */
public class DataResAdapter1 extends RecyclerView.Adapter<DataResAdapter1.DataHolder> implements ItemClickListener {

    private final Context context;
    private DataresItem dataresFood;

    public DataResAdapter1(DataresItem dataFood, Context context) {
        this.context = context;
        this.dataresFood = dataFood;
    }

    @Override
    public DataResAdapter1.DataHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemLayoutView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.datares, null);

        final DataResAdapter1.DataHolder viewHolder = new DataResAdapter1.DataHolder(itemLayoutView,this,context);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(DataResAdapter1.DataHolder holder, int position) {

//        holder.imageView.setImageResource(home1.getImgeView());
        Picasso.with(context)
                .load(dataresFood.getDataRestaurant().get(position).getImage())

                .into(holder.imageView);

        holder.textView_name.setText(dataresFood.getDataRestaurant().get(position).getName());
    }

    @Override
    public int getItemCount() {
        return dataresFood.getDataRestaurant().size();
    }

    @Override
    public void onItemClick(View view, int position) {

        Intent intent = new Intent(context, DataRestaurantActivity.class);
        Bundle bundle = new Bundle();
        bundle.putString("imgFood1", dataresFood.getDataRestaurant().get(position).getImage());
        bundle.putString("nameFood1", dataresFood.getDataRestaurant().get(position).getName());
        intent.putExtras(bundle);
        context.startActivity(new Intent(context, BuffetActivity.class));

    }

    public class DataHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        private final ItemClickListener mitemClickListener;
        private final Context context;
        private final TextView textView_name;
        public ImageView imageView;



        public DataHolder(View itemView,ItemClickListener itemClickListener,Context context) {
            super(itemView);
            this.context=context;

            imageView = (ImageView) itemView.findViewById(R.id.imggen);
            textView_name = (TextView) itemView.findViewById(R.id.textView_name1);
            mitemClickListener = itemClickListener;
            itemView.setOnClickListener((View.OnClickListener) this);
        }


        @Override
        public void onClick(View v) {
            mitemClickListener.onItemClick(v,getPosition());
        }
    }
}
