package com.example.myfristapp.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.myfristapp.Item.VideoItem;
import com.example.myfristapp.R;
import com.squareup.picasso.Picasso;

/**
 * Created by DELL on 3/18/2016.
 */
public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.RecyclerHolder>{

    private VideoItem data;
    private Context context;
    public RecyclerAdapter(VideoItem dataFood, Context context) {
        this.data = dataFood;
        this.context = context;
    }



    @Override
    public RecyclerHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler,parent,false);

        return new RecyclerHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerHolder holder, int position) {

        //holder.imageView.setImageResource(data.getDatalist().getCuteVdoLists().get(position).getVdoPic());
        holder.textView.setText(data.getDatalist().getCuteVdoLists().get(position).getVdoName());

if (!data.getDatalist().getCuteVdoLists().get(position).getVdoPic().equals("")){
    Picasso.with(context)
            .load(data.getDatalist().getCuteVdoLists().get(position).getVdoPic())
            .error(R.drawable.cafe)
            .into(holder.imageView);

}else {
    holder.imageView.setImageResource(R.drawable.cafe);

}

    }



    @Override
    public int getItemCount() {
        return data.getDatalist().getCuteVdoLists().size();
    }

    public class RecyclerHolder extends RecyclerView.ViewHolder {

        private ImageView imageView;
        private TextView textView;

        public RecyclerHolder(View itemView) {
            super(itemView);

            imageView = (ImageView) itemView.findViewById(R.id.imgrecycler);
            textView = (TextView) itemView.findViewById(R.id.textView);


        }
    }





}
