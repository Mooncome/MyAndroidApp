package com.example.myfristapp.Adapter;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.example.myfristapp.Activity.BuffetActivity;
import com.example.myfristapp.Activity.GeneralActivity;
import com.example.myfristapp.ItemClickListener;
import com.example.myfristapp.R;
import com.example.myfristapp.Retrofit.ResImageItem;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by DELL on 7/12/2016.
 */
public class ResAdapter extends RecyclerView.Adapter<ResAdapter.ResHolder> implements ItemClickListener {

    private final Context context;
    private ArrayList<ResImageItem> dataFood;
    private int userId;
    private String fristname;

    public ResAdapter(ArrayList<ResImageItem> dataFood,Context context) {

        this.dataFood = dataFood;
        this.context = context;


    }

    @Override
    public ResAdapter.ResHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemLayoutView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.res, null);

        final ResAdapter.ResHolder viewHolder = new ResAdapter.ResHolder(itemLayoutView,this);
        return viewHolder;



    }



    @Override
    public void onBindViewHolder(ResAdapter.ResHolder holder, int position) {
        ResImageItem home1 = dataFood.get(position);
//        holder.imageView.setImageResource(home1.getImgeView());
        Picasso.with(context)
                .load(home1.getImgeView())

                .into(holder.imageView);
    }

    @Override
    public int getItemCount() {

        return dataFood.size();
    }



    @Override
    public void onItemClick(View view, int position) {

        if (dataFood.get(position).getType().equals("buffet")){
            //Toast.makeText(ResAdapter.this,"",Toast.LENGTH_SHORT).show();

           context. startActivity(new Intent(context, BuffetActivity.class));

        }else {
            context. startActivity(new Intent(context, GeneralActivity.class));
        }



    }


    public class ResHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public ImageView imageView;
        public ItemClickListener mitemClickListener;

        public ResHolder(View itemView,ItemClickListener itemClickListener) {
            super(itemView);

            imageView = (ImageView) itemView.findViewById(R.id.res_layout);
            mitemClickListener = itemClickListener;
            itemView.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {
            mitemClickListener.onItemClick(v,getPosition());
        }
    }
}
