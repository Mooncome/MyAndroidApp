package com.example.myfristapp.Adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.myfristapp.Activity.DataRestaurantActivity;

import com.example.myfristapp.Item.GeneralImageItem;
import com.example.myfristapp.Item.VideoItem;
import com.example.myfristapp.ItemClickListener;
import com.example.myfristapp.R;
import com.example.myfristapp.Retrofit.DataRestaurant;
import com.example.myfristapp.Retrofit.DataresItem;
import com.example.myfristapp.Retrofit.ImageDataItem;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class GeneralAdapter extends RecyclerView.Adapter<GeneralAdapter.GenHolder> implements ItemClickListener {

        private final Context context;
        private DataresItem data;

        public GeneralAdapter(DataresItem dataFood,Context context) {
            this.context = context;
            this.data = dataFood;
        }



    @Override
        public GenHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemLayoutView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.general, null);

        final GeneralAdapter.GenHolder viewHolder = new GeneralAdapter.GenHolder(itemLayoutView,this,context);
        return viewHolder;
        }

        @Override
        public void onBindViewHolder(GenHolder holder, int position) {
             holder.textView_name.setText(data.getDataRestaurant().get(position).getName());
//    holder.imageView.setImageResource(data.getDataRestaurant().get(position).getGetimage());
            // holder.imageView.setImageResource(data.getImageDatares().get(position).getName());
                if (!data.getDataRestaurant().get(position).getImage().equals("")) {
                    Picasso.with(context)
                            .load(data.getDataRestaurant().get(position).getImage())
                            .error(R.drawable.delete)
                            .into(holder.imageView);
                }

        }

        @Override
        public int getItemCount() {
            return data.getDataRestaurant().size();
        }

        @Override
        public void onItemClick(View view, int position) {
            Intent intent = new Intent(context, DataRestaurantActivity.class);
            Bundle bundle = new Bundle();
            bundle.putInt("idrestauranFood", data.getDataRestaurant().get(position).getIdrestauran());
            bundle.putInt("idtypeFood", data.getDataRestaurant().get(position).getIdtype());
            bundle.putString("imgFood", data.getDataRestaurant().get(position).getImage());
            bundle.putString("nameFood", data.getDataRestaurant().get(position).getName());
            bundle.putString("addFood",data.getDataRestaurant().get(position).getAdd());
            bundle.putString("telFood",data.getDataRestaurant().get(position).getTel());
            bundle.putInt("minFood", data.getDataRestaurant().get(position).getPriceMin());
            bundle.putInt("maxFood", data.getDataRestaurant().get(position).getPriceMax());
            bundle.putString("mapFood", data.getDataRestaurant().get(position).getMap());
            intent.putExtras(bundle);
            context.startActivity(intent);
        }

        public class GenHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
            public ImageView imageView;
            public ItemClickListener mitemClickListener;
            public TextView textView_name;
            public Context context;


            public GenHolder(View itemView,ItemClickListener itemClickListener,Context context) {
                super(itemView);
                this.context=context;

                imageView = (ImageView) itemView.findViewById(R.id.imggen1);
                textView_name = (TextView) itemView.findViewById(R.id.textView_name2);
                mitemClickListener = itemClickListener;
                itemView.setOnClickListener(this);
            }

            @Override
            public void onClick(View v) {
                mitemClickListener.onItemClick(v,getPosition());

            }
        }
}

