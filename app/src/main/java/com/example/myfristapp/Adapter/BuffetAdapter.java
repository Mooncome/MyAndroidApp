package com.example.myfristapp.Adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.myfristapp.Activity.DataRestaurantActivity;
import com.example.myfristapp.Item.BuffetImageItem;
import com.example.myfristapp.ItemClickListener;
import com.example.myfristapp.R;
import com.example.myfristapp.Retrofit.DataBuffet;
import com.example.myfristapp.Retrofit.DatabuffetItem;
import com.example.myfristapp.Retrofit.DataresItem;
import com.example.myfristapp.Retrofit.MenubuffetImageItem;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by DELL on 7/12/2016.
 */
public class BuffetAdapter extends RecyclerView.Adapter<BuffetAdapter.BufHolder> implements ItemClickListener {

    private final Context context;
    private DatabuffetItem data;

    public BuffetAdapter(DatabuffetItem dataFood,Context context) {
        this.context = context;
        this.data = dataFood;
    }



    @Override
    public BufHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemLayoutView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.buffet, null);

        final BuffetAdapter.BufHolder viewHolder = new BuffetAdapter.BufHolder(itemLayoutView,this,context);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(BufHolder holder, int position) {
        holder.textView_name.setText(data.getDataBuffet().get(position).getName());
//            holder.imageView.setImageResource(data.getDataRestaurant().get(position).getGetimage());
        // holder.imageView.setImageResource(data.getImageDatares().get(position).getName());
        if (!data.getDataBuffet().get(position).getImage().equals("")) {//ดักapiที่ไม่มีรูป
            Picasso.with(context)
                    .load(data.getDataBuffet().get(position).getImage())
                    .error(R.drawable.delete)
                    .into(holder.imageView);
        }

    }


    @Override
    public int getItemCount() {

        return data.getDataBuffet().size();
    }

    @Override
    public void onItemClick(View view, int position) {

        Intent intent = new Intent(context, DataRestaurantActivity.class);
        Bundle bundle = new Bundle();
        bundle.putInt("idrestauranFood1", data.getDataBuffet().get(position).getIdrestauran());
        bundle.putInt("idtypeFood1", data.getDataBuffet().get(position).getIdtype());
        bundle.putString("imgFood", data.getDataBuffet().get(position).getImage());
        bundle.putString("nameFood", data.getDataBuffet().get(position).getName());
        bundle.putString("addFood",data.getDataBuffet().get(position).getAdd());
        bundle.putString("telFood",data.getDataBuffet().get(position).getTel());
        bundle.putInt("minFood", data.getDataBuffet().get(position).getPriceMin());
        bundle.putInt("maxFood", data.getDataBuffet().get(position).getPriceMax());
        bundle.putString("mapFood",data.getDataBuffet().get(position).getMap());
        intent.putExtras(bundle);
        context.startActivity(intent);

    }

    public class BufHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        public ImageView imageView;
        public ItemClickListener mitemClickListener;
        public TextView textView_name;
        public Context context;

        public BufHolder(View itemView,ItemClickListener itemClickListener,Context context) {
            super(itemView);
            this.context=context;

            imageView = (ImageView) itemView.findViewById(R.id.imgbuf);
            textView_name = (TextView) itemView.findViewById(R.id.textView_name);
            mitemClickListener = itemClickListener;
            itemView.setOnClickListener(this);
        }



        @Override
        public void onClick(View v) {

            mitemClickListener.onItemClick(v,getPosition());
        }
    }
}
