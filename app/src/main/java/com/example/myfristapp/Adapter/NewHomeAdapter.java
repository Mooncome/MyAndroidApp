package com.example.myfristapp.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.myfristapp.NewHomeItem;
import com.example.myfristapp.R;

import java.util.ArrayList;

/**
 * Created by DELL on 4/6/2016.
 */
public class NewHomeAdapter extends RecyclerView.Adapter<NewHomeAdapter.NewHomeHolder> {

    private ArrayList<NewHomeItem> Food;
    private Context context;

    public NewHomeAdapter(ArrayList<NewHomeItem> dataFood,Context context) {

        this.Food = dataFood;
        this.context = context;


    }


    @Override
    public NewHomeAdapter.NewHomeHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.newhome,parent,false);

        return new NewHomeHolder(view);
    }

    @Override
    public void onBindViewHolder(NewHomeAdapter.NewHomeHolder holder, int position) {
        NewHomeItem home = Food.get(position);
        holder.imageView.setImageResource(home.getImgeView());
        holder.textView.setText(home.getTitle());
    }

    @Override
    public int getItemCount() {
        return 0;
    }

    public class NewHomeHolder extends RecyclerView.ViewHolder {
        public ImageView imageView;
        public TextView textView;

        public NewHomeHolder(View itemView) {
            super(itemView);
            imageView = (ImageView) itemView.findViewById(R.id.imgnewhome1);
            textView = (TextView) itemView.findViewById(R.id.tv_1);
            imageView = (ImageView) itemView.findViewById(R.id.imgnewhome2);
        }
    }
}
