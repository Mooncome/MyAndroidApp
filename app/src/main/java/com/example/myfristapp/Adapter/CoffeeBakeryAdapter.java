package com.example.myfristapp.Adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;

import com.example.myfristapp.R;

/**
 * Created by DELL on 2/16/2016.
 */
public class CoffeeBakeryAdapter extends BaseAdapter {

    private Context ctx;

    public CoffeeBakeryAdapter(Context c) {
        ctx = c;
    }




    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return pics.length;
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        ImageView iv;
        if (convertView == null) {  // if it's not recycled, initialize some attributes
            iv = new ImageView(ctx);
            iv.setLayoutParams(new GridView.LayoutParams(700, 350));
            iv.setScaleType(ImageView.ScaleType.CENTER_CROP);
            iv.setPadding(8,8,8,8);
        } else {
            iv = (ImageView) convertView;
        }

        iv.setImageResource(pics[position]);
        return iv;

    }

    private Integer[] pics = {
            R.drawable.ba, R.drawable.ba1,
            R.drawable.ba1, R.drawable.ba,
            R.drawable.ba, R.drawable.ba1,
            R.drawable.ba1, R.drawable.ba,
            R.drawable.ba, R.drawable.ba1,
            R.drawable.ba1, R.drawable.ba,


    };
}
