package com.example.myfristapp.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.myfristapp.ItemClickListener;
import com.example.myfristapp.R;
import com.example.myfristapp.Retrofit.ImageItem;
import com.squareup.picasso.Picasso;

/**
 * Created by DELL on 7/22/2016.
 */
public class DataImageAdapter extends RecyclerView.Adapter<DataImageAdapter.DataHolder> implements ItemClickListener{
    private final Context context;
    private ImageItem data;
    public DataImageAdapter(ImageItem dataFood,Context context) {
        this.context = context;
        this.data = dataFood;
    }

    @Override
    public DataImageAdapter.DataHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemLayoutView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.datares, null);

        final DataImageAdapter.DataHolder viewHolder = new DataImageAdapter.DataHolder(itemLayoutView,this,context);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(DataHolder holder, int position) {
        holder.textView_name.setText(data.getImageData().get(position).getMenuname());
//

        Picasso.with(context)
                .load(data.getImageData().get(position).getMenuameimage())
                .error(R.drawable.delete)
                .into(holder.imageView);
    }




    @Override
    public int getItemCount() {
        return data.getImageData().size();
    }

    @Override
    public void onItemClick(View view, int position) {


    }

    public class DataHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        private final ItemClickListener mitemClickListener;
        private final Context context;
        private final TextView textView_name;
        public ImageView imageView;



        public DataHolder(View itemView,ItemClickListener itemClickListener,Context context) {
            super(itemView);
            this.context=context;

            imageView = (ImageView) itemView.findViewById(R.id.imggen);
            textView_name = (TextView) itemView.findViewById(R.id.textView_name1);
            mitemClickListener = itemClickListener;
            itemView.setOnClickListener((View.OnClickListener) this);
        }


        @Override
        public void onClick(View v) {

        }
    }
}


