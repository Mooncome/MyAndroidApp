package com.example.myfristapp.Adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.myfristapp.Activity.BuffetActivity;
import com.example.myfristapp.Activity.DataRestaurantActivity;
import com.example.myfristapp.Activity.MenuActivity;
import com.example.myfristapp.Item.DataresImageItem;
import com.example.myfristapp.ItemClickListener;
import com.example.myfristapp.R;
import com.example.myfristapp.Retrofit.DataresItem;
import com.example.myfristapp.Retrofit.MenuImageItem;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by DELL on 7/13/2016.
 */
public class DataResAdapter extends RecyclerView.Adapter<DataResAdapter.DataHolder> implements ItemClickListener {

    private final Context context;
    private MenuImageItem data;
    private MenuImageItem dataFood;

    public DataResAdapter(MenuImageItem dataFood,Context context) {
        this.context = context;
        this.data = dataFood;
    }

    @Override
    public DataResAdapter.DataHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemLayoutView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.datares, null);

        final DataResAdapter.DataHolder viewHolder = new DataResAdapter.DataHolder(itemLayoutView,this,context);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(DataResAdapter.DataHolder holder, int position) {
        holder.textView_name.setText(data.getDataMenu().get(position).getName());
//

        Picasso.with(context)
                .load(data.getDataMenu().get(position).getPhoto())
                .error(R.drawable.delete)
                .into(holder.imageView);


//        holder.textView_name.setText(dataFood.getDataMenu().get(position).getName());
//        if (!dataFood.getDataMenu().get(position).getPhoto().equals("")) {
//            Picasso.with(context)
//                    .load(dataFood.getDataMenu().get(position).getPhoto())
//                    .error(R.drawable.delete)
//                    .into(holder.imageView);
//        }

    }



    @Override
    public int getItemCount() {
        return data.getDataMenu().size();
    }

    @Override
    public void onItemClick(View view, int position) {
        Intent intent = new Intent(context, MenuActivity.class);
        Bundle bundle = new Bundle();
        bundle.putString("imgFood1", data.getDataMenu().get(position).getPhoto());
        bundle.putString("nameFood1", String.valueOf(data.getDataMenu().get(position).getName()));
//        bundle.putString("idtypeFood1", data.getDataMenu().get(position).getIdtype());
//        bundle.putString("idrestauranFood1", String.valueOf(data.getDataMenu().get(position).getIdrestaurant()));
        intent.putExtras(bundle);
        context.startActivity(intent);


    }

    public class DataHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        private final ItemClickListener mitemClickListener;
        private final Context context;
        private final TextView textView_name;
        public ImageView imageView;



        public DataHolder(View itemView,ItemClickListener itemClickListener,Context context) {
            super(itemView);
            this.context=context;

            imageView = (ImageView) itemView.findViewById(R.id.imggen);
            textView_name = (TextView) itemView.findViewById(R.id.textView_name1);
            mitemClickListener = itemClickListener;
            itemView.setOnClickListener((View.OnClickListener) this);
        }


        @Override
        public void onClick(View v) {
            mitemClickListener.onItemClick(v, getPosition());

        }
    }
}
