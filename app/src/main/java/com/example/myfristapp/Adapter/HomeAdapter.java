package com.example.myfristapp.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.example.myfristapp.Item.HomeImageItem;
import com.example.myfristapp.R;

import java.util.ArrayList;


public class HomeAdapter extends RecyclerView.Adapter<HomeAdapter.HomeHolder> implements View.OnClickListener{


    private ArrayList<HomeImageItem> dataFood;
    private Context context;

    public HomeAdapter(ArrayList<HomeImageItem> dataFood,Context context) {

        this.dataFood = dataFood;
        this.context = context;


    }



//    public void addListHome(HomeImageItem home){
//
//        homeFragments.add(home);
//        notifyDataSetChanged();
//
//    }

    @Override
    public HomeHolder onCreateViewHolder(ViewGroup parent, int viewType) {


        View itemLayoutView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.home, null);

        final HomeAdapter.HomeHolder viewHolder = new HomeAdapter.HomeHolder(itemLayoutView);
        return viewHolder;


    }

    @Override
    public void onBindViewHolder(HomeHolder holder, int position) {

        HomeImageItem home1 = dataFood.get(position);
        holder.imageView.setImageResource(home1.getImgeView());





    }

    @Override
    public int getItemCount() {
        return dataFood.size();
    }

    @Override
    public void onClick(View v) {

    }

    public class HomeHolder extends RecyclerView.ViewHolder {

        private  ImageView imageView;


        public HomeHolder(View itemView) {
            super(itemView);

            imageView = (ImageView) itemView.findViewById(R.id.imghome);



        }
    }
}
