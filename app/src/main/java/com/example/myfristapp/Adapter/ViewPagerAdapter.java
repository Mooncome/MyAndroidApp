package com.example.myfristapp.Adapter;
import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.example.myfristapp.R;


public class ViewPagerAdapter extends PagerAdapter {
    private Context context;
    private BannerItem imgData;

    private LayoutInflater mLayoutInflater;

    public ViewPagerAdapter(Context context, BannerItem imgData) {
        this.context = context;
        this.imgData = imgData;
        mLayoutInflater = LayoutInflater.from(context);
    }


    @Override
    public int getCount() {
        return imgData.getBannerDataItem().size();
    }


    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((ImageView) object);
    }



    @Override
    public Object instantiateItem(ViewGroup container, final int position) {
        final ImageView imageView;

        mLayoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View itemView = mLayoutInflater.inflate(R.layout.item_viewpager_layout, container,false);


        imageView = (ImageView) itemView.findViewById(R.id.image_view);


            imageView.setImageResource(R.drawable.ba1);


        // Add viewpager_item.xml to ViewPager
        ((ViewPager) container).addView(itemView);



        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        ((ViewPager) container).removeView((ImageView) object);
    }




}
