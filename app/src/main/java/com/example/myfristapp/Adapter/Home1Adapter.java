package com.example.myfristapp.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.example.myfristapp.Activity.BuffetActivity;
import com.example.myfristapp.Activity.GeneralActivity;
import com.example.myfristapp.Fragment.RestaurantFragment;
import com.example.myfristapp.HomeImageItem1;
import com.example.myfristapp.ItemClickListener;
import com.example.myfristapp.Fragment.NearbyFragment;
import com.example.myfristapp.R;


import java.util.ArrayList;

/**
 * Created by DELL on 4/8/2016.
 */
public class Home1Adapter extends RecyclerView.Adapter<Home1Adapter.HomeHolder> implements ItemClickListener {

    private ArrayList<HomeImageItem1> dataFood;
    private Context context;

    public Home1Adapter(ArrayList<HomeImageItem1> dataFood,Context context) {


        this.dataFood = dataFood;
        this.context = context;


    }



//    public void addListHome(HomeImageItem home){
//
//        homeFragments.add(home);
//        notifyDataSetChanged();
//
//    }

    @Override
    public Home1Adapter.HomeHolder onCreateViewHolder(ViewGroup parent, int viewType) {


        View itemLayoutView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.home1, null);

        final Home1Adapter.HomeHolder viewHolder = new Home1Adapter.HomeHolder(itemLayoutView,this);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(Home1Adapter.HomeHolder holder, int position) {

        HomeImageItem1 home1 = dataFood.get(position);
        holder.imageView.setImageResource(home1.getImgeView());



    }

    @Override
    public int getItemCount() {
        return dataFood.size();
    }



    @Override
    public void onItemClick(View view, int position) {
        if (position==0){
            context. startActivity(new Intent(context, BuffetActivity.class));
        }else {
            context. startActivity(new Intent(context, GeneralActivity.class));

        }
    }


    public class HomeHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        private  ItemClickListener mitemClickListener;
        private  ImageView imageView;


        public HomeHolder(View itemView,ItemClickListener itemClickListener) {
            super(itemView);
            mitemClickListener = itemClickListener;
            imageView = (ImageView) itemView.findViewById(R.id.imghome1);

            itemView.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {
            mitemClickListener.onItemClick(v,getPosition());
        }
    }
}
