package com.example.myfristapp;

import com.google.gson.annotations.SerializedName;

public class Shot {
    private int id;
    private String title;
    private String description;
    private String url;

    @SerializedName("image_url")
    private String imageUrl;

    public String getUrl() {
        return url;
    }

    public String getTitle() {
        return title;
    }
}
