package com.example.myfristapp;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.myfristapp.Adapter.NewHomeAdapter;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 */
public class NewHomeFragment extends Fragment {

    private NewHomeAdapter newHomeAdapter;
    private RecyclerView recyclerView;
    public NewHomeFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_new_home,container,false);
        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view1);

        ArrayList<NewHomeItem> homeList = new ArrayList<>();
        homeList.add(new NewHomeItem(R.drawable.t1,"food"));
        homeList.add(new NewHomeItem(R.drawable.t2,"food"));

        recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 3));
        newHomeAdapter = new NewHomeAdapter(homeList,getActivity());
        recyclerView.setAdapter(newHomeAdapter);


        return view;

    }

}