package com.example.myfristapp.Item;

/**
 * Created by DELL on 3/17/2016.
 */
public class RecyclerItem {
    public int imgeView;
    public  String title;

    public RecyclerItem(int imgeView, String title) {
        this.imgeView = imgeView;
        this.title = title;
    }

    public int getImgeView() {
        return imgeView;
    }

    public String getTitle() {

        return title;
    }

}
