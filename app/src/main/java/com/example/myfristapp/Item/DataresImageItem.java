package com.example.myfristapp.Item;

/**
 * Created by DELL on 7/13/2016.
 */
public class DataresImageItem {

    public int imgeView;
    public String name;
    private String add;
    private String tel;
    private int pricemin;
    private int pricemax;
    private String map;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public DataresImageItem(int imgeView,String name) {
        this.imgeView = imgeView;
        this.name = name;

    }


    public int getImgeView() {

        return imgeView;
    }

    public String getAdd() {
        return add;
    }

    public String getTel() {
        return tel;
    }

    public int getPricemin() {
        return pricemin;
    }

    public int getPricemax() {
        return pricemax;
    }

    public String getMap() {
        return map;
    }
}
