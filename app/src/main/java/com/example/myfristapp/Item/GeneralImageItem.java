package com.example.myfristapp.Item;

/**
 * Created by DELL on 7/12/2016.
 */
public class GeneralImageItem {
    public int imgeView;
    public String name;


    public GeneralImageItem(int imgeView,String name) {
        this.imgeView = imgeView;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getImgeView() {
        return imgeView;
    }


}
