package com.example.myfristapp.BusService;

import android.os.Handler;
import android.os.Looper;
import android.support.v4.app.Fragment;
import android.util.Log;

import com.squareup.otto.Bus;

/**
 * Created by layer on 17/6/2558.
 */
public class BaseBusFragment extends Fragment {
    private static final String TAG = "BaseBusFragment";
    @Override
    public void onResume() {
        super.onResume();
        try {
            Log.i(TAG, "Regisbus Fragment");
            getBus().register(this);
        }catch (IllegalArgumentException e){
            e.printStackTrace();
        }catch (RuntimeException er){
            er.printStackTrace();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        getBus().unregister(this);
    }

    private Bus mBus;

    protected Bus getBus() {
        if (mBus == null) {
            mBus = BusProvider.getInstance();
        }
        return mBus;
    }

    protected void setBus(Bus bus) {
        mBus = bus;
    }

    private final Handler mainThread = new Handler(Looper.getMainLooper());

    public void postOnMain(final Object event) {
        if (Looper.myLooper() == Looper.getMainLooper()) {
            getBus().post(event);
        } else {
            mainThread.post(new Runnable() {
                @Override
                public void run() {
                    getBus().post(event);

                }
            });
        }
    }
}