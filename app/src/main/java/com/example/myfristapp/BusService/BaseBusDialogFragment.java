package com.example.myfristapp.BusService;

import android.support.v4.app.DialogFragment;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import com.squareup.otto.Bus;

/**
 * Created by Jirat on 12/24/2015.
 */
public class BaseBusDialogFragment extends DialogFragment {

    private static final String TAG = "BaseBusDialogFragment";
    @Override
    public void onResume() {
        super.onResume();
        try {
            Log.i(TAG, "Regisbus Fragment");
            getBus().register(this);
        }catch (IllegalArgumentException e){
            e.printStackTrace();
        }catch (RuntimeException er){
            er.printStackTrace();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        getBus().unregister(this);
    }

    private Bus mBus;

    protected Bus getBus() {
        if (mBus == null) {
            mBus = BusProvider.getInstance();
        }
        return mBus;
    }

    protected void setBus(Bus bus) {
        mBus = bus;
    }

    private final Handler mainThread = new Handler(Looper.getMainLooper());

    public void postOnMain(final Object event) {
        if (Looper.myLooper() == Looper.getMainLooper()) {
            getBus().post(event);
        } else {
            mainThread.post(new Runnable() {
                @Override
                public void run() {
                    getBus().post(event);

                }
            });
        }
    }

}
