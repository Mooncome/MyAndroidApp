package com.example.myfristapp.BusService;

import android.os.Handler;
import android.os.Looper;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.squareup.otto.Bus;

/**
 * Created by layer on 10/10/2558.
 */
public class BaseBusActivity extends AppCompatActivity {
    private static final String TAG = "BaseBusActivity";
/*    @Override
    public void onCreate(Bundle savedInstanceState, PersistableBundle persistentState) {
        super.onCreate(savedInstanceState, persistentState);
        getBus().register(this);
    }*/

    @Override
    protected void onStart() {
        super.onStart();
        Log.i(TAG, "Regisbus Activity");
        getBus().register(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        getBus().unregister(this);
    }

/*    @Override
    protected void onDestroy() {
        super.onDestroy();
        getBus().unregister(this);
    }*/

/*    @Override
         protected void onResume() {
        super.onResume();
        getBus().register(this);
    }*/

/*    @Override
    protected void onPause() {
        super.onPause();
        getBus().unregister(this);
    }*/

    private Bus mBus;

    protected Bus getBus() {
        if (mBus == null) {
            mBus = BusProvider.getInstance();
        }
        return mBus;
    }

    protected void setBus(Bus bus) {
        mBus = bus;
    }

    private final Handler mainThread = new Handler(Looper.getMainLooper());

    public void postOnMain(final Object event) {
        if (Looper.myLooper() == Looper.getMainLooper()) {
            getBus().post(event);
        } else {
            mainThread.post(new Runnable() {
                @Override
                public void run() {
                    getBus().post(event);
                }
            });
        }
    }

}
