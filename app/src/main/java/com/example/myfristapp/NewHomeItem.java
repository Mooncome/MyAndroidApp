package com.example.myfristapp;

/**
 * Created by DELL on 4/6/2016.
 */
public class NewHomeItem {

        public int imgeView;
        public  String title;

        public NewHomeItem(int imgeView, String title) {
            this.imgeView = imgeView;
            this.title = title;
        }

        public int getImgeView() {
            return imgeView;
        }

        public String getTitle() {
            return title;
        }

}
