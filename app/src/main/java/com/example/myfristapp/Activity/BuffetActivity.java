package com.example.myfristapp.Activity;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;

import com.example.myfristapp.Adapter.BuffetAdapter;
import com.example.myfristapp.Adapter.GeneralAdapter;
import com.example.myfristapp.Adapter.ResAdapter;
import com.example.myfristapp.ApplicationConfig;
import com.example.myfristapp.Item.BuffetImageItem;
import com.example.myfristapp.R;
import com.example.myfristapp.Retrofit.DatabuffetItem;
import com.example.myfristapp.Retrofit.DataresItem;
import com.example.myfristapp.Retrofit.ResImageItem;
import com.example.myfristapp.Retrofit.SimpleRetrofit;
import com.example.myfristapp.SampleAdapter;
import com.example.myfristapp.SampleImageItem;

import java.util.ArrayList;

import retrofit.Call;
import retrofit.Callback;
import retrofit.GsonConverterFactory;
import retrofit.Response;
import retrofit.Retrofit;

public class BuffetActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private BuffetAdapter bufAdapter;
    private Context activity;
    private String map;
    private int idrestauran;
    private int idtype;
    private String name;
    private String add;
    private String tel;
    private int pricemin;
    private int pricemax;
    public String image;
    private SampleAdapter samAdapter1;
    private int userId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_buffet);

        recyclerView = (RecyclerView)findViewById(R.id.recycler_view);

        SharedPreferences sp = getSharedPreferences("User_ID", Context.MODE_PRIVATE);
        userId = sp.getInt("User_ID",-1);
//        ArrayList<SampleImageItem> resList = new ArrayList<SampleImageItem>();
//        resList.add(new SampleImageItem(R.drawable.t1));
//        resList.add(new SampleImageItem(R.drawable.t2));
//        resList.add(new SampleImageItem(R.drawable.t3));
//        resList.add(new SampleImageItem(R.drawable.t4));
//
//        recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 1));
//        samAdapter1 =  new SampleAdapter(resList,getActivity());
//        recyclerView.setAdapter(samAdapter1);



        Retrofit client = new Retrofit.Builder()
                .baseUrl(ApplicationConfig.URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        SimpleRetrofit simpleRetrofit = client.create(SimpleRetrofit.class);
        Call<DatabuffetItem> call = simpleRetrofit.getRestaurantbuffet(map, idrestauran, idtype, name, add, tel, pricemin, pricemax, image);
        call.enqueue(new Callback<DatabuffetItem>() {
            @Override
            public void onResponse(Response<DatabuffetItem> response) {
                Log.d("", String.valueOf(response.raw()));

                Log.d("", String.valueOf(response.body()));

                if (response.body() != null) {
                    initRecycleGridView(response.body());


                }
            }

            private void initRecycleGridView(DatabuffetItem body) {

                recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 2));

                BuffetAdapter bufAdapter = new BuffetAdapter(body, BuffetActivity.this);
                recyclerView.setAdapter(bufAdapter);
            }

            @Override
            public void onFailure(Throwable t) {
                Log.d("", String.valueOf(t));
            }

        });
    }


    public Context getActivity() {
        return activity;
    }
}
