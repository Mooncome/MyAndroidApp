package com.example.myfristapp.Activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.example.myfristapp.ApplicationConfig;
import com.example.myfristapp.Fragment.OrderAdapter;
import com.example.myfristapp.Fragment.OrderListadapter;
import com.example.myfristapp.R;
import com.example.myfristapp.Retrofit.DatabuffetItem;
import com.example.myfristapp.Retrofit.OrderItem;
import com.example.myfristapp.Retrofit.SimpleRetrofit;

import java.util.ArrayList;
import java.util.List;

import retrofit.Call;
import retrofit.Callback;
import retrofit.GsonConverterFactory;
import retrofit.Response;
import retrofit.Retrofit;


public class OrderListActivity extends Activity {
    private RecyclerView recyclerView;
    private int id ;
    private String status;
    private String name;
    private int userId;
    private Button button_confirm,button_cancel;
    private String token;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_orderlist);

        recyclerView = (RecyclerView) findViewById(R.id.recyclerview_order);
        button_confirm = (Button) findViewById(R.id.button_confirm);
        button_cancel= (Button) findViewById(R.id.button_cancel);

        SharedPreferences sp = getSharedPreferences("User_ID", Context.MODE_PRIVATE);
        userId = sp.getInt("User_ID",-1);
        name = sp.getString("First_Name", "");
        token = sp.getString("newToken","");
        Intent intent = this.getIntent();
        Bundle extra = intent.getExtras();

        if (extra != null) {
            id = extra.getInt("id");
            status = extra.getString("status");

        }
        if (status.equals("USER")){
            getOderUser(userId,id);
            button_confirm.setVisibility(View.GONE);
            button_cancel.setVisibility(View.GONE);
            //ซ่อน

        }else {
            getOderUser(id,5);
            button_confirm.setVisibility(View.VISIBLE);
            button_cancel.setVisibility(View.VISIBLE);
        }




        button_confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getConfirm("5");
            }
        });

        button_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getCancel("5");
            }
        });
    }

    private void getOderUser(int userId,int resId){
        Retrofit client = new Retrofit.Builder()
                .baseUrl(ApplicationConfig.URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        SimpleRetrofit simpleRetrofit = client.create(SimpleRetrofit.class);
        Call<OrderItem> call = simpleRetrofit.getOrderFoodWithId(userId,resId);
        call.enqueue(new Callback<OrderItem>() {
            @Override
            public void onResponse(Response<OrderItem> response) {
                Log.d("", String.valueOf(response.raw()));

                Log.d("", String.valueOf(response.body()));

                if (response.body() != null) {





                    initRecycleGridView(response.body());

                }
            }

            @Override
            public void onFailure(Throwable t) {

            }

            private void initRecycleGridView(OrderItem body) {
                recyclerView.setLayoutManager(new GridLayoutManager(OrderListActivity.this, 1));

                OrderListadapter orderAdapter = new OrderListadapter(body, OrderListActivity.this);
                recyclerView.setAdapter(orderAdapter);
            }


        });


    }

    private void getConfirm(String userId ){
        Retrofit client = new Retrofit.Builder()
                .baseUrl(ApplicationConfig.URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        SimpleRetrofit simpleRetrofit = client.create(SimpleRetrofit.class);
        Call<DatabuffetItem> call = simpleRetrofit.pushconfirm("Rock Me Burger&bar",token);
        call.enqueue(new Callback<DatabuffetItem>() {
            @Override
            public void onResponse(Response<DatabuffetItem> response) {
                Log.d("", String.valueOf(response.raw()));

                Toast.makeText(OrderListActivity.this,"Confirm this order success",Toast.LENGTH_SHORT).show();

            }

            @Override
            public void onFailure(Throwable t) {
                Toast.makeText(OrderListActivity.this,"Can not confirm this order",Toast.LENGTH_SHORT).show();
            }



        });


    }
    private void getCancel(String userId ){
        Retrofit client = new Retrofit.Builder()
                .baseUrl(ApplicationConfig.URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        SimpleRetrofit simpleRetrofit = client.create(SimpleRetrofit.class);
        Call<DatabuffetItem> call = simpleRetrofit.pushcancel("Rock Me Burger&bar",token);
        call.enqueue(new Callback<DatabuffetItem>() {
            @Override
            public void onResponse(Response<DatabuffetItem> response) {
                Log.d("", String.valueOf(response.raw()));

                Toast.makeText(OrderListActivity.this,"Cancel this order success",Toast.LENGTH_SHORT).show();

            }

            @Override
            public void onFailure(Throwable t) {
                Toast.makeText(OrderListActivity.this,"Can not confirm this order",Toast.LENGTH_SHORT).show();
            }



        });


    }







}
