package com.example.myfristapp.Activity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.myfristapp.R;
import com.kosalgeek.android.photoutil.CameraPhoto;
import com.kosalgeek.android.photoutil.GalleryPhoto;
import com.kosalgeek.android.photoutil.ImageLoader;

import java.io.FileNotFoundException;
import java.io.IOException;

/**
 * Created by DELL on 11/28/2016.
 */
public class CustomDialog extends Activity{
    private  final  String TAG = this.getClass().getName();


    ImageView ivcamera,ivgallory;
    CameraPhoto cameraPhoto;
    GalleryPhoto galleryPhoto;

    final int CAMERA_REQUEST = 13323;
    final int GALLERY_REQUEST = 12232;
    private ImageView img_view;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.customdialog);

//        img_view = (ImageView)findViewById(R.id.imageView);

        cameraPhoto = new CameraPhoto(getApplicationContext());
        galleryPhoto = new GalleryPhoto(getApplicationContext());

        ivcamera = (ImageView)findViewById(R.id.image_carmera);
        ivgallory = (ImageView)findViewById(R.id.image_gallory);

        ivcamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                try {
//                    startActivityForResult(cameraPhoto.takePhotoIntent(), CAMERA_REQUEST);
//                    cameraPhoto.addToGallery();
//                } catch (IOException e) {
//                    Toast.makeText(getApplicationContext(),
//                            "Somthing Wrong while taking photos", Toast.LENGTH_SHORT).show();
//
//                }

            }
        });

        ivgallory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(galleryPhoto.openGalleryIntent(), GALLERY_REQUEST);
            }
        });


    }


    protected void onActivityResult(int requestCode, int resultCode, Intent imageReturnedIntent) {
        if(resultCode == RESULT_OK){
            if(resultCode == CAMERA_REQUEST){
                String photopath = cameraPhoto.getPhotoPath();
                try{
                    Bitmap bitmap = ImageLoader.init().from(photopath).requestSize(512,512).getBitmap();
                    img_view.setImageBitmap(bitmap);
                } catch (FileNotFoundException e) {
                    Toast.makeText(getApplicationContext(),
                            "Somthing Wrong while taking photos", Toast.LENGTH_SHORT).show();

                }
            }
            else if (requestCode == GALLERY_REQUEST){
                String photopath = galleryPhoto.getPath();
                try{
                    Bitmap bitmap = ImageLoader.init().from(photopath).requestSize(512,512).getBitmap();
                    img_view.setImageBitmap(bitmap);
                } catch (FileNotFoundException e) {
                    Toast.makeText(getApplicationContext(),
                            "Somthing Wrong while choosing photos", Toast.LENGTH_SHORT).show();

                }

            }
        }
    }

}
