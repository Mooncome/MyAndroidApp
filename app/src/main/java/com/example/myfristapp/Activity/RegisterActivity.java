package com.example.myfristapp.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Toast;

import com.example.myfristapp.ApplicationConfig;
import com.example.myfristapp.R;
import com.example.myfristapp.Retrofit.RegisterItem;
import com.example.myfristapp.Retrofit.SimpleRetrofit;

import retrofit.Call;
import retrofit.Callback;
import retrofit.GsonConverterFactory;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * Created by DELL on 2/11/2016.
 */
public class RegisterActivity extends AppCompatActivity implements View.OnClickListener{

    Button btn_register;
    EditText et_fristname,et_add,et_email,et_username, et_password,et_confirm;
    String fristname,add,email,username, password,confirm,id,status;
    public CheckBox checkBox1,checkBox2;
    public static final String MyPREFERENCES = "MyPrefsFile";
    private CompoundButton checkBox;
    private String user;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        btn_register = (Button)findViewById(R.id.btn_register);


        checkBox1 = (CheckBox) findViewById(R.id.cb_user);
        checkBox2 = (CheckBox) findViewById(R.id.cb_res);
        et_fristname = (EditText) findViewById(R.id.et_fristname);
        et_add = (EditText) findViewById(R.id.et_add);
        et_email = (EditText) findViewById(R.id.et_email);
        et_username = (EditText) findViewById(R.id.et_username);
        et_password = (EditText) findViewById(R.id.et_password);

//        et_confirm = (EditText) findViewById(R.id.et_confirm);


        btn_register.setOnClickListener(this);

        status="USER";
        checkBox1.setChecked(true);
        checkBox1.setSelected(true);




        checkBox1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView,
                                         boolean isChecked) {

                if (buttonView.isChecked()) {
                    checkBox2.setChecked(false);
                    checkBox2.setSelected(false);
                    status="USER";
                }

            }

        });

        checkBox2.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView,
                                         boolean isChecked) {

                if (buttonView.isChecked()) {
                    checkBox1.setChecked(false);
                    checkBox1.setSelected(false);

                    status="RESTAURANT";
                }

            }

        });


    }


//    public void onCheckboxClicked(View view) {
//
//        switch(view.getId()) {
//
//            case R.id.cb_user:
//
//                checkBox2.setChecked(false);
//
//
//                break;
//
//            case R.id.cb_res:
//
//                checkBox1.setChecked(false);
//
//                break;
//
//
//        }
//    }
    int status1;
    String num_status;

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_register:

                fristname = et_fristname.getText().toString();
                add = et_add.getText().toString();
                email = et_email.getText().toString();
                username = et_username.getText().toString();
                password = et_password.getText().toString();



                Retrofit client = new Retrofit.Builder()
                        .baseUrl(ApplicationConfig.URL)
                        .addConverterFactory(GsonConverterFactory.create())
                        .build();
                final SimpleRetrofit simpleRetrofit = client.create(SimpleRetrofit.class);
                Call<RegisterItem> call = simpleRetrofit.getRegister(fristname, add, email, username, password, status);
                call.enqueue(new Callback<RegisterItem>() {
                    @Override
                    public void onResponse(Response<RegisterItem> response) {

                        Log.d("dd", response.raw().toString());

                        if (response != null) {

                            if (response.body().getMessage().equals("สำเร็จ")) {
                                Toast.makeText(RegisterActivity.this, "Register Success", Toast.LENGTH_LONG).show();
                                startActivity(new Intent(RegisterActivity.this, LoginActivity.class));

                            } else {
                                Toast.makeText(RegisterActivity.this, "Username s already taken. Please try again", Toast.LENGTH_LONG).show();

                            }


                        }


                    }

                    @Override
                    public void onFailure(Throwable t) {

                        Toast.makeText(RegisterActivity.this, "Register Failed", Toast.LENGTH_LONG).show();

                    }
                });

        break;

    }
    }
}
