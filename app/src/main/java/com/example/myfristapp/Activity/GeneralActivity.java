package com.example.myfristapp.Activity;

    import android.app.Activity;
    import android.content.Context;
    import android.content.SharedPreferences;
    import android.os.Bundle;
    import android.support.v7.widget.GridLayoutManager;
    import android.support.v7.widget.RecyclerView;
    import android.support.v7.widget.Toolbar;
    import android.util.Log;

    import com.example.myfristapp.Adapter.GeneralAdapter;
    import com.example.myfristapp.Adapter.RecyclerAdapter;
    import com.example.myfristapp.ApplicationConfig;
    import com.example.myfristapp.Fragment.GeneralguidAdapter;
    import com.example.myfristapp.Item.GeneralImageItem;
    import com.example.myfristapp.Retrofit.DataresItem;
    import com.example.myfristapp.Retrofit.ImageDataItem;
    import com.example.myfristapp.R;
    import com.example.myfristapp.Retrofit.SimpleRetrofit;

    import java.util.ArrayList;

    import retrofit.Call;
    import retrofit.Callback;
    import retrofit.GsonConverterFactory;
    import retrofit.Response;
    import retrofit.Retrofit;

/**
     * Created by DELL on 7/12/2016.
     */
    public class GeneralActivity extends Activity {

        private RecyclerView recyclerView;
        private GeneralAdapter genAdapter;
        private Context activity;
        private Toolbar supportActionBar;
    private String map;
    private int idrestauran;
    private int idtype;
    private String name;
    private String add;
    private String tel;
    private int pricemin;
    private int pricemax;
    private int idrestaurantimage;
    private int idrestaurant;
    private String image;
    private int userId;


    @Override
        protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_general);

        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);

        SharedPreferences sp = getSharedPreferences("User_ID", Context.MODE_PRIVATE);
        userId = sp.getInt("User_ID",-1);


        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);




        Retrofit client = new Retrofit.Builder()
                .baseUrl(ApplicationConfig.URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        SimpleRetrofit simpleRetrofit = client.create(SimpleRetrofit.class);
        Call<DataresItem> call = simpleRetrofit.getRestaurant();
        call.enqueue(new Callback<DataresItem>() {
            @Override
            public void onResponse(Response<DataresItem> response) {
                Log.d("", String.valueOf(response.raw()));

                Log.d("", String.valueOf(response.body()));

                if (response.body() != null) {
                    initRecycleGridView(response.body());


                }
            }

            private void initRecycleGridView(DataresItem body) {

//                int restaurant_id = body.getDataRestaurant().get()
                recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 2));

                GeneralAdapter genAdapter = new GeneralAdapter(body, GeneralActivity.this);
                recyclerView.setAdapter(genAdapter);
            }


            @Override
            public void onFailure(Throwable t) {
                Log.d("", String.valueOf(t));
            }
        });



    }





    public Context getActivity() {
            return activity;
        }


        public void setSupportActionBar(Toolbar supportActionBar) {
            this.supportActionBar = supportActionBar;
        }

        public Toolbar getSupportActionBar() {
            return supportActionBar;
        }
    }