package com.example.myfristapp.Activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.myfristapp.ApplicationConfig;
import com.example.myfristapp.Datalogin.Loginstatus;
import com.example.myfristapp.R;
import com.example.myfristapp.Retrofit.SimpleRetrofit;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.firebase.iid.FirebaseInstanceId;

import retrofit.Call;
import retrofit.Callback;
import retrofit.GsonConverterFactory;
import retrofit.Response;
import retrofit.Retrofit;


public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    TextView tv_register;
    Button btn_login;
    EditText edt_username, edt_pass;

    String username, password;
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        tv_register = (TextView) findViewById(R.id.tv_register);
        btn_login = (Button) findViewById(R.id.btn_login);
        edt_username = (EditText) findViewById(R.id.et_username);
        edt_pass = (EditText) findViewById(R.id.et_password);

        tv_register.setOnClickListener(this);
        btn_login.setOnClickListener(this);


        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
    }

    @Override
    public void onClick(final View v) {
        switch (v.getId()) {
            case R.id.tv_register:

                startActivity(new Intent(this, RegisterActivity.class));

                break;
            case R.id.btn_login:


//                startActivity(new Intent(LoginActivity.this, HomeActivity.class));
               connectLogin();

                break;
        }
    }
    public void connectLogin() {
        username = edt_username.getText().toString();
        password = edt_pass.getText().toString();
      /*  username ="natnon";
        password = "12345";*/

    Retrofit client = new Retrofit.Builder()
            .baseUrl(ApplicationConfig.URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build();
    final SimpleRetrofit simpleRetrofit = client.create(SimpleRetrofit.class);
    Call<Loginstatus> call = simpleRetrofit.getLogin(username,password);
    call.enqueue(new Callback<Loginstatus>() {
        @Override
        public void onResponse(Response<Loginstatus> response) {
            Log.d("dd", response.raw().toString());

            response.message();

            if (response.message()!= null ) {
//
                if (response.body().getMessage().equals("สำเร็จ")){
                      Log.d("dd", response.body().getData().get(0).getUsername());
                    Toast.makeText(LoginActivity.this,"Login Success : "+response.body().getData().get(0).getUsername(), Toast.LENGTH_LONG).show();
                    String newToken = FirebaseInstanceId.getInstance().getToken();
                        startActivity(new Intent(LoginActivity.this, HomeActivity.class));
                    SharedPreferences sp = getSharedPreferences("User_ID", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = sp.edit();
                    editor.putInt("User_ID", response.body().getData().get(0).getIduser());
                    editor.putString("First_Name", response.body().getData().get(0).getFirstname());
                    editor.putString("Add", response.body().getData().get(0).getAdd());
                    editor.putString("E_mail", response.body().getData().get(0).getEmail());
                    editor.putString("status",response.body().getData().get(0).getStatus());
                    editor.putString("newToken", FirebaseInstanceId.getInstance().getToken());

                    editor.commit();




                    Log.d("LoginActivity", ("getGcmToken :" + newToken));
                }else {
                Toast.makeText(LoginActivity.this,"Login Failed. Please try again",Toast.LENGTH_LONG).show();
               }

            }



        }

        @Override
        public void onFailure(Throwable t) {
//            Log.d("onFailure", t.getMessage());

            Toast.makeText(LoginActivity.this,"Please connect the internet",Toast.LENGTH_LONG).show();

        }
    });
}


}
