package com.example.myfristapp.Activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.myfristapp.ApplicationConfig;
import com.example.myfristapp.BusService.BusProvider;
import com.example.myfristapp.ItemClickListener;
import com.example.myfristapp.R;
import com.example.myfristapp.Retrofit.DataMenuFood;
import com.example.myfristapp.Retrofit.DatabuffetItem;
import com.example.myfristapp.Retrofit.DataresItem;
import com.example.myfristapp.Retrofit.MenuFoodItem;
import com.example.myfristapp.Retrofit.MenuImageItem;
import com.example.myfristapp.Retrofit.MenuOrderItem;
import com.example.myfristapp.Retrofit.OrderData;
import com.example.myfristapp.Retrofit.RegisterItem;
import com.example.myfristapp.Retrofit.SimpleRetrofit;
import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;
import com.squareup.picasso.Picasso;

import java.text.CollationElementIterator;
import java.util.ArrayList;

import retrofit.Call;
import retrofit.Callback;
import retrofit.GsonConverterFactory;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * Created by DELL on 11/16/2016.
 */
public class MenuresAdapter extends RecyclerView.Adapter<MenuresAdapter.MenuHolder> implements ItemClickListener {

    private final Context context;

    ArrayList<DataMenuFood> dataMenuFoods;
    /*private ArrayList<DataMenuFood> orderdata ;*/
    private ArrayList<OrderData> orderdata ;
    private int idmenuorder;
    private int idrestaurant;
    private String token;
    private int quantity;
    private int iduser;
    private Intent intent;
    private String img_food1;
    private String fristname;


    public MenuresAdapter(Context context, ArrayList<DataMenuFood> dataMenuFoods, int iduser,String fristname,String token) {
        this.context = context;
        this.dataMenuFoods = dataMenuFoods;
        this.iduser = iduser;
        this.fristname = fristname;
        this.token = token;
        orderdata = new ArrayList<OrderData>();
    }




    @Override
    public MenuresAdapter.MenuHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemLayoutView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.menures, null);

        final MenuresAdapter.MenuHolder viewHolder = new MenuresAdapter.MenuHolder (itemLayoutView,this,context);
        return viewHolder;



    }

    @Override
    public void onBindViewHolder(final MenuresAdapter.MenuHolder holder, final int position) {
//        holder.imageView.setImageResource(home1.getImgeView());

        holder.textView_name.setText(dataMenuFoods.get(position).getName());
        if (!dataMenuFoods.get(position).getPhoto().equals("")) {
            Picasso.with(context)
                    .load(dataMenuFoods.get(position).getPhoto())
                    .error(R.drawable.delete)
                    .into(holder.imageView);
        }





//        Picasso.with(context)
//                .load(dataFood.getDataMenu().get(position).getPhoto())
//
//                .into(holder.imageView);

//        holder.textView_name.setText(String.valueOf(dataFood.getDataMenu().get(position).getName()));
    }

    @Override
    public int getItemCount() {
        return dataMenuFoods.size();
    }

    @Override
    public void onItemClick(View view, int position) {
//        Intent intent = new Intent(context, DataRestaurantActivity.class);
//        Bundle bundle = new Bundle();
//        bundle.putString("imgFood1", dataFood.getDataMenu().get(position).getPhoto());
//        bundle.putString("nameFood1", String.valueOf(dataFood.getDataMenu().get(position).getName()));
//        intent.putExtras(bundle);
//        context.startActivity(intent);
//        Intent intent = this.getIntent();
//        Bundle extra = intent.getExtras();
//
//        if (extra!=null) {
//            fristname = extra.getString("First_Name");
//        }

    }

    public class MenuHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        public ItemClickListener mitemClickListener;
        public Context context;
        public TextView textView_name;
        public ImageView imageView;
        public CheckBox checkBox;
        private EditText textView_quantity;




        public MenuHolder(View itemView,ItemClickListener itemClickListener,Context context) {
            super(itemView);
            this.context=context;
            checkBox = (CheckBox) itemView.findViewById(R.id.checkBox);
            imageView = (ImageView) itemView.findViewById(R.id.imgmenu);
            textView_name = (TextView) itemView.findViewById(R.id.textView_namemenu);
            textView_quantity = (EditText) itemView.findViewById(R.id.quantity);
            mitemClickListener = itemClickListener;
            itemView.setOnClickListener(this);


            checkBox.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int quantity;
                    String num_quantity;
                    if (checkBox.isChecked()){


                         num_quantity= textView_quantity.getText().toString();

                        if (num_quantity.equals("")){
                             quantity =1;
                        }else {
                             quantity = Integer.valueOf(num_quantity);
                        }
                        textView_quantity.setEnabled(false);

                        orderdata.add(new OrderData(dataMenuFoods.get(getAdapterPosition()).getIdmenu(), dataMenuFoods.get(getAdapterPosition()).getIdrestaurant(), dataMenuFoods.get(getAdapterPosition()).getName(), quantity, dataMenuFoods.get(getAdapterPosition()).getRestaurantname()));

                        Log.d("checkBox", "isChecked : " + dataMenuFoods.get(getAdapterPosition()).getName());

                        Log.d("checkBox", "OrderData : " + orderdata.toString());
                    }else {

                        num_quantity= textView_quantity.getText().toString();
                        textView_quantity.setEnabled(true);

                        if (num_quantity.equals("")){
                            quantity =1;
                        }else {
                            quantity = Integer.valueOf(num_quantity);
                        }
                        Log.d("checkBox", "isNotChecked : " + dataMenuFoods.get(getAdapterPosition()).getName());
                        orderdata.remove(new OrderData(dataMenuFoods.get(getAdapterPosition()).getIdmenu(), dataMenuFoods.get(getAdapterPosition()).getIdrestaurant(), dataMenuFoods.get(getAdapterPosition()).getName(), quantity,dataMenuFoods.get(getAdapterPosition()).getRestaurantname()));



                        Log.d("checkBox", "OrderData : " + orderdata.toString());

                    }
                }
            });

        }

        @Override
        public void onClick(View v) {
            mitemClickListener.onItemClick(v, getPosition());


        }
    }

    private void sendOrder(int idmenuorder, int idrestaurant, String menuname, int quantity,int iduser,String username,String restaurantname){

        Retrofit client = new Retrofit.Builder()
                .baseUrl(ApplicationConfig.URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        final SimpleRetrofit simpleRetrofit = client.create(SimpleRetrofit.class);
        Call<MenuOrderItem> call = simpleRetrofit.getOrderdata(idmenuorder, idrestaurant, menuname, quantity,iduser,username,restaurantname);
        call.enqueue(new Callback<MenuOrderItem>() {

            @Override
            public void onResponse(Response<MenuOrderItem> response) {
                Log.d("dd", response.raw().toString());

                if (response != null) {
                    orderdata.clear();
//                    if (response.body().getMessage().equals("สำเร็จ")) {
//                        Toast.makeText(context, "Register Success", Toast.LENGTH_LONG).show();
//
//                    } else {
//                        Toast.makeText(context, "Username s already taken. Please try again", Toast.LENGTH_LONG).show();
//
//                    }


                }


            }

            @Override
            public void onFailure(Throwable t) {

            }
        });
    }

    @Subscribe
    public void receiveSend(MenuActivity.sendOrder even) {
        //not used

        Log.d("ff", "ffffffffffff");
        Log.d("ff", "orderdata : " + orderdata.size());


        pushOrder(String.valueOf(iduser));

        if (orderdata.size()>0){
            for (int i =0;i<orderdata.size();i++){

                sendOrder(orderdata.get(i).getIdmenuorder(),orderdata.get(i).getIdrestaurant(),orderdata.get(i).getMenuname(),orderdata.get(i).getQuantity(),iduser,fristname,orderdata.get(i).getRestaurantname());

            }
        }else {
            Toast.makeText(context,"",Toast.LENGTH_SHORT).show();
        }

//        กรุณาเลือกเมนูอาหาร
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
        getBus().register(this);
    }

    @Override
    public void onDetachedFromRecyclerView(RecyclerView recyclerView) {
        super.onDetachedFromRecyclerView(recyclerView);
        getBus().unregister(this);
    }
    private Bus mBus;

    protected Bus getBus() {
        if (mBus == null) {
            mBus = BusProvider.getInstance();
        }
        return mBus;
    }

    protected void setBus(Bus bus) {
        mBus = bus;
    }

    private final Handler mainThread = new Handler(Looper.getMainLooper());

    public void postOnMain(final Object event) {
        if (Looper.myLooper() == Looper.getMainLooper()) {
            getBus().post(event);
        } else {
            mainThread.post(new Runnable() {
                @Override
                public void run() {
                    getBus().post(event);
                }
            });
        }
    }
    private void pushOrder(String userId){
        Retrofit client = new Retrofit.Builder()
                .baseUrl(ApplicationConfig.URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        SimpleRetrofit simpleRetrofit = client.create(SimpleRetrofit.class);
        Call<DatabuffetItem> call = simpleRetrofit.pushSendId(userId,token);
        call.enqueue(new Callback<DatabuffetItem>() {
            @Override
            public void onResponse(Response<DatabuffetItem> response) {
                Log.d("", String.valueOf(response.raw()));

                Toast.makeText(context,"Send this order success",Toast.LENGTH_SHORT).show();

            }

            @Override
            public void onFailure(Throwable t) {
                Toast.makeText(context,"Can not Send this order",Toast.LENGTH_SHORT).show();
            }



        });


    }

    public Intent getIntent() {
        return intent;
    }
}
