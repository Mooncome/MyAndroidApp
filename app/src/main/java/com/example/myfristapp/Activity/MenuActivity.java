package com.example.myfristapp.Activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.menu.MenuAdapter;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.myfristapp.Adapter.DataImageAdapter;
import com.example.myfristapp.Adapter.DataResAdapter1;
import com.example.myfristapp.ApplicationConfig;
import com.example.myfristapp.BusService.BusProvider;
import com.example.myfristapp.R;
import com.example.myfristapp.Retrofit.DataMenuFood;
import com.example.myfristapp.Retrofit.DataresItem;
import com.example.myfristapp.Retrofit.ImageItem;
import com.example.myfristapp.Retrofit.MenuFoodItem;
import com.example.myfristapp.Retrofit.MenuImageItem;
import com.example.myfristapp.Retrofit.SimpleRetrofit;
import com.google.firebase.iid.FirebaseInstanceId;
import com.squareup.okhttp.internal.framed.FrameReader;
import com.squareup.otto.Bus;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import retrofit.Call;
import retrofit.Callback;
import retrofit.GsonConverterFactory;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * Created by DELL on 8/29/2016.
 */
public class MenuActivity extends AppCompatActivity implements View.OnClickListener{

    private RecyclerView recyclerView;
    private Context activity;
    private TextView textView_name;
    private ImageView imageView_food;
    private String img_food1,img_food2;
    private String name_food1,name_food2;
    private int idrestauranFood1;
    private int idtypeFood1;
    private Button btt_submit;
    private int userId;
    private View logTokenButton;
    private static final String TAG = "MenuActivity";
    private String name;
    private String token;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.activity_menu);
        super.onCreate(savedInstanceState);

//         CheckBox checkBox = (CheckBox) findViewById(R.id.checkBox);
//        final Button b = (Button)findViewById(R.id.btt_submit);

        recyclerView = (RecyclerView) findViewById(R.id.recycler_viewmenu);
        btt_submit = (Button) findViewById(R.id.btt_submit);
        if (getIntent().getExtras() != null) {
            for (String key : getIntent().getExtras().keySet()) {
                String value = getIntent().getExtras().getString(key);
                Log.d(TAG, "Key: " + key + " Value: " + value);
            }
        }

        btt_submit.setOnClickListener(this);


        Intent intent = this.getIntent();
        Bundle extra = intent.getExtras();

        if (extra!=null){
            img_food1 = extra.getString("imgFood1");
            name_food1 =extra.getString("nameFood1");
            idtypeFood1 = extra.getInt("idtypeFood1");
            idrestauranFood1 =extra.getInt("idrestauranFood1");
        }

        imageView_food = (ImageView) findViewById(R.id.imgmenu);
        textView_name = (TextView) findViewById(R.id.textView_namemenu);


        SharedPreferences sp = getSharedPreferences("User_ID", Context.MODE_PRIVATE);
        userId = sp.getInt("User_ID", -1);
        name = sp.getString("First_Name","");
        token = sp.getString("newToken","");


//        Picasso.with(this)
//                .load(img_food1)
//                .into(imageView_food);
//
//
//
//        textView_name.setText(name_food1);

//        checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener(){
//            @Override
//            public void onCheckedChanged(CompoundButton buttonView,
//                                         boolean isChecked) {
//
//                if (buttonView.isChecked()) {
//                    b.setEnabled(true);
//                } else {
//                    b.setEnabled(false);
//                }
//
//            }
//
//        });


        Retrofit client = new Retrofit.Builder()
                .baseUrl(ApplicationConfig.URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        SimpleRetrofit simpleRetrofit = client.create(SimpleRetrofit.class);

        Call<MenuFoodItem> call = simpleRetrofit.getDataMenuFood();
        call.enqueue(new Callback<MenuFoodItem>() {
            @Override
            public void onResponse(Response<MenuFoodItem> response) {
                Log.d("", String.valueOf(response.raw()));

                Log.d("", String.valueOf(response.body()));

                ArrayList<DataMenuFood> dataMenuFoods = new ArrayList<DataMenuFood>();

                if (response.body() != null) {

                    for (int i =0;i<response.body().getDataMenuFood().size();i++){
                        if (response.body().getDataMenuFood().get(i).getIdrestaurant()==idrestauranFood1&&response.body().getDataMenuFood().get(i).getIdtype()==idtypeFood1){

                            dataMenuFoods.add(response.body().getDataMenuFood().get(i));
                        }


                    }

                    initRecycleGridView(dataMenuFoods);

                }
            }

            @Override
            public void onFailure(Throwable t) {
                Log.d("", String.valueOf(t));
            }


        });


    }

    private void initRecycleGridView(ArrayList<DataMenuFood> dataMenuFoods) {
        recyclerView.setLayoutManager(new GridLayoutManager(MenuActivity.this, 1));

        MenuresAdapter menuAdapter = new MenuresAdapter(MenuActivity.this, dataMenuFoods,userId,name,token);
        recyclerView.setAdapter(menuAdapter);

    }

    @Override
    protected void onStart() {
        super.onStart();
        getBus().register(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        getBus().unregister(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btt_submit:
                Log.d(TAG, "InstanceID token: " + FirebaseInstanceId.getInstance().getToken());

                getBus().post(new sendOrder());
                startActivity(new Intent(this, HomeActivity.class));



                break;
        }





    }

    private Bus mBus;

    protected Bus getBus() {
        if (mBus == null) {
            mBus = BusProvider.getInstance();
        }
        return mBus;
    }

    protected void setBus(Bus bus) {
        mBus = bus;
    }

    private final Handler mainThread = new Handler(Looper.getMainLooper());

    public void postOnMain(final Object event) {
        if (Looper.myLooper() == Looper.getMainLooper()) {
            getBus().post(event);
        } else {
            mainThread.post(new Runnable() {
                @Override
                public void run() {
                    getBus().post(event);
                }
            });
        }
    }

    public class sendOrder{

    }
}
