package com.example.myfristapp.Activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.myfristapp.Adapter.DataImageAdapter;
import com.example.myfristapp.Adapter.DataResAdapter;
import com.example.myfristapp.Adapter.DataResAdapter1;
import com.example.myfristapp.ApplicationConfig;
import com.example.myfristapp.R;
import com.example.myfristapp.Retrofit.DataresItem;
import com.example.myfristapp.Retrofit.ImageItem;
import com.example.myfristapp.Retrofit.MenuImageItem;
import com.example.myfristapp.Retrofit.SimpleRetrofit;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.squareup.picasso.Picasso;

import retrofit.Call;
import retrofit.Callback;
import retrofit.GsonConverterFactory;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * Created by DELL on 7/13/2016.
 */
public class DataRestaurantActivity extends FragmentActivity implements View.OnClickListener, OnMapReadyCallback {

    private RecyclerView recyclerView, recyclerView1;
    private DataResAdapter1 dataAdapter;
    private Context activity;
    private String img_food;
    private String name_food;
    private ImageView imageView_food;
    private TextView textView_name;
    private String add_food;
    private String tel_food;
    private int min_food;
    private int max_food;
    private String map_food;
    private TextView textView_add;
    private TextView textView_tel;
    private TextView textView_min;
    private TextView textView_mix;
    private MapView mMapView;
    private GoogleMap googleMap;
    private View map;
    private int idrestauran;
    private int idtype;
    private String name;
    private String add;
    private String tel;
    private int pricemin;
    private int pricemax;
    private String image;
    private int idmenu;
    private int idrestaurant;
    private String price;
    private String photo;
    private int drestaurant;
    Button button;
    private String img_food1;
    private String name_food1;
    private int idtypeFood1;
    private int idrestauranFood1;
    private int userId;
    private ImageView imageview;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.restaurant);

        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        recyclerView1 = (RecyclerView) findViewById(R.id.recycler_view1);
        imageview = (ImageView) findViewById(R.id.imageButton);
        button = (Button) findViewById(R.id.bttorder);

        imageview.setOnClickListener(this);


        button.setOnClickListener(this);

        Intent intent = this.getIntent();
        Bundle extra = intent.getExtras();

        SharedPreferences sp = getSharedPreferences("User_ID", Context.MODE_PRIVATE);
        userId = sp.getInt("User_ID",-1);

        if (extra != null) {
            img_food = extra.getString("imgFood");
            name_food = extra.getString("nameFood");
            add_food = extra.getString("addFood");
            tel_food = extra.getString("telFood");
            min_food = extra.getInt("minFood");
            max_food = extra.getInt("maxFood");
            map_food = extra.getString("mapFood");
            idtype = extra.getInt("idtypeFood");
            drestaurant = extra.getInt("idrestauranFood");
        }
//        if (extra!=null){
//            img_food1 = extra.getString("imgFood1");
//            name_food1 =extra.getString("nameFood1");
//        }

        imageView_food = (ImageView) findViewById(R.id.image_res);
        textView_name = (TextView) findViewById(R.id.textView_name1);
        textView_add = (TextView) findViewById(R.id.tv_add);
        textView_tel = (TextView) findViewById(R.id.tv_tel);
        textView_min = (TextView) findViewById(R.id.tv_pricemin);
        textView_mix = (TextView) findViewById(R.id.tv_pricemax);
//        textView_map = (TextView)findViewById(R.id.mapView1);


        Picasso.with(this)
                .load(img_food)
                .into(imageView_food);


        textView_name.setText(name_food);
        textView_add.setText(add_food);
        textView_tel.setText(tel_food);
        textView_min.setText("" + min_food);
        textView_mix.setText("" + max_food);


        Retrofit client = new Retrofit.Builder()
                .baseUrl(ApplicationConfig.URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        SimpleRetrofit simpleRetrofit = client.create(SimpleRetrofit.class);

        Call<MenuImageItem> call = simpleRetrofit.getMenuImageItem();
        call.enqueue(new Callback<MenuImageItem>() {
            @Override
            public void onResponse(Response<MenuImageItem> response) {
                Log.d("", String.valueOf(response.raw()));

                Log.d("", String.valueOf(response.body()));

                if (response.body() != null) {
                    initRecycleGridView(response.body());


                }
            }

            private void initRecycleGridView(MenuImageItem body) {
                recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 2));
                DataResAdapter dataAdapter = new DataResAdapter(body, DataRestaurantActivity.this);
                recyclerView.setAdapter(dataAdapter);


            }

            @Override
            public void onFailure(Throwable t) {
                Log.d("", String.valueOf(t));
            }
        });


//        Retrofit client = new Retrofit.Builder()
//                .baseUrl(ApplicationConfig.URL)
//                .addConverterFactory(GsonConverterFactory.create())
//                .build();
//        SimpleRetrofit simpleRetrofit = client.create(SimpleRetrofit.class);
//
//        Call<DataresItem> call = simpleRetrofit.getRestaurant();
//        call.enqueue(new Callback<DataresItem>() {
//            @Override
//            public void onResponse(Response<DataresItem> response) {
//                Log.d("", String.valueOf(response.raw()));
//
//                Log.d("", String.valueOf(response.body()));
//
//                if (response.body() != null) {
//
//                    //initRecycleGridView(response.body());
//
//                }
//            }
//
//            private void initRecycleGridView(DataresItem body) {
//                recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 2));
//
//                DataResAdapter1 dataAdapter = new DataResAdapter1(body, DataRestaurantActivity.this);
//                recyclerView.setAdapter(dataAdapter);
//            }
//
//
//            @Override
//            public void onFailure(Throwable t) {
//                Log.d("", String.valueOf(t));
//            }
//        });

        client = new Retrofit.Builder()
                .baseUrl(ApplicationConfig.URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        simpleRetrofit = client.create(SimpleRetrofit.class);
        Call<ImageItem> callre = simpleRetrofit.getImageview(drestaurant, idtype);
        callre.enqueue(new Callback<ImageItem>() {
            @Override
            public void onResponse(Response<ImageItem> response) {
//            Log.d("dd", response.raw().toString());
                Log.d("", String.valueOf(response.raw()));

                Log.d("", String.valueOf(response.body()));

                if (response.body().getImageData() != null) {
                    initRecycleGridView(response.body());


                }
            }

            private void initRecycleGridView(ImageItem body) {
                recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 2));

                DataImageAdapter dataAdapter = new DataImageAdapter(body, DataRestaurantActivity.this);
                recyclerView.setAdapter(dataAdapter);

            }


            //Log.d("dd", response.body().getData().get(0).getUsername());

//                            startActivity(new Intent(LoginActivity.this, HomeActivity.class));
//                            Toast.makeText(LoginActivity.this,"Logged in", Toast.LENGTH_LONG).show();


            @Override
            public void onFailure(Throwable t) {

            }
        });
//        MapFragment mapFragment = (MapFragment) getFragmentManager()
//                .findFragmentById(R.id.map);

//        mapFragment.getMapAsync(this);
        map = findViewById(R.id.map);
       SupportMapFragment mapFragment = (SupportMapFragment)this.getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
       /* mMapView = (MapView) findViewById(R.id.map);
        mMapView.onCreate(savedInstanceState);

        mMapView.onResume();// needed to get the map to display immediately

        try {
            MapsInitializer.initialize(getActivity().getApplicationContext());
        } catch (Exception e) {
            e.printStackTrace();
        }
*/

     /*   String[] parts = map_food.split(",");
        String partLatitude = parts[0];
        String partLongitude = parts[1];

//        googleMap = mMapView.getMap();
        // latitude and longitude
        double latitude = Double.parseDouble(partLatitude);
        double longitude = Double.parseDouble(partLongitude);

        // create marker
        MarkerOptions marker = new MarkerOptions().position(
                new LatLng(latitude, longitude)).title("");

        // Changing marker icon
        marker.icon(BitmapDescriptorFactory
                .defaultMarker(BitmapDescriptorFactory.HUE_ROSE));

        // adding marker
        googleMap.addMarker(marker);
        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(new LatLng(latitude, longitude)).zoom(15).build();
        googleMap.animateCamera(CameraUpdateFactory
                .newCameraPosition(cameraPosition));

        // Perform any camera updates here
        return;*/

    }

    public Context getActivity() {
        return activity;
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.bttorder:
                Intent intent = new Intent(DataRestaurantActivity.this, MenuActivity.class);

                Bundle bundle = new Bundle();
                MenuImageItem data;
                int position;
//                bundle.putString("idtypeFood1", data.getDataMenu().get(position).getIdtype());
//                bundle.putString("idrestauranFood1", String.valueOf(data.getDataMenu().get(position).getIdrestaurant()));
                bundle.putInt("idtypeFood1", idtype);
                bundle.putInt("idrestauranFood1", drestaurant);
                intent.putExtras(bundle);

                startActivity(intent);

                break;

            case R.id.imageButton:

                Toast.makeText(DataRestaurantActivity.this, "ถูกใจแล้ว", Toast.LENGTH_SHORT).show();

                break;
        }


    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        String[] parts = map_food.split(",");
        String partLatitude = parts[0];
        String partLongitude = parts[1];

//        googleMap = mMapView.getMap();
        // latitude and longitude
        double latitude = Double.parseDouble(partLatitude);
        double longitude = Double.parseDouble(partLongitude);

        // create marker
        MarkerOptions marker = new MarkerOptions().position(
                new LatLng(latitude, longitude)).title("");

        // Changing marker icon
        marker.icon(BitmapDescriptorFactory
                .defaultMarker(BitmapDescriptorFactory.HUE_ROSE));

        // adding marker
        googleMap.addMarker(marker);
        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(new LatLng(latitude, longitude)).zoom(15).build();
        googleMap.animateCamera(CameraUpdateFactory
                .newCameraPosition(cameraPosition));

        // Perform any camera updates here
    }
}

