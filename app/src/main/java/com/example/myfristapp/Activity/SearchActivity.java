package com.example.myfristapp.Activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.example.myfristapp.ApplicationConfig;
import com.example.myfristapp.R;
import com.example.myfristapp.Retrofit.DataMenuFood;
import com.example.myfristapp.Retrofit.DatabuffetItem;
import com.example.myfristapp.Retrofit.OrderItem;
import com.example.myfristapp.Retrofit.ReviewItem;
import com.example.myfristapp.Retrofit.SearchItem;
import com.example.myfristapp.Retrofit.SimpleRetrofit;

import java.util.ArrayList;
import java.util.Arrays;

import retrofit.Call;
import retrofit.Callback;
import retrofit.GsonConverterFactory;
import retrofit.Response;
import retrofit.Retrofit;

public class SearchActivity extends Activity {
    String[] items;
    ArrayList<String > listitems;
    ArrayAdapter<String> adapter;
    ListView listView;
    EditText editText;
    private String restaurantname;
    private int restaurant_id;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_search);
//        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
//        setSupportActionBar(toolbar);

        listView=(ListView)findViewById(R.id.listview_search);
        editText=(EditText)findViewById(R.id.txtsearch);
        initList();



        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                Log.d("SearchActivity", "start : "+String.valueOf(start));
                Log.d("SearchActivity", "count : "+String.valueOf(count));
                Log.d("SearchActivity", "CharSequence : "+String.valueOf(s));

                if (start<1){
                    listView.setVisibility(View.GONE);
                }else if (start<1&&count==0){
                    listView.setVisibility(View.GONE);
                }else {
                    listView.setVisibility(View.VISIBLE);
                }

                searchItem(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

    }

    public void  searchItem(String textToSearch){
        for (String item:items){
            if (!item.contains(textToSearch)){
                listitems.remove(item);
            }
        }
        adapter.notifyDataSetChanged();
    }

    public  void  initList(){
        listView.setVisibility(View.GONE);
        items=new  String[]{"Khunmor cuisine คุณหมอคูซีน","Mu's Katsu","Rock Me Burger&bar","ร้านมีนา","Aisushi Chiang Mai","ร้านน้ำดอกไม้","เฮือนม่วนใจ","เฮือนใจ๋ยอง","เฮือนเพ็ญ","GINZI สุกี้+จุ่มแซ่บ","Retro Steak Cafe","Yamato Japanese Buffet","อันดามัน ซีฟู๊ด","ดิบดี Sushi Café้","เThe Buffet","Horizon Village Resort & Conference Centre"};
        listitems=new ArrayList<>(Arrays.asList(items));
        adapter=new ArrayAdapter<String>(this,R.layout.list_item,R.id.txtitem,listitems);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {


                String value = (String) listView.getItemAtPosition(position);

                Toast.makeText(SearchActivity.this,value, Toast.LENGTH_LONG).show();

                Retrofit client = new Retrofit.Builder()
                        .baseUrl(ApplicationConfig.URL)
                        .addConverterFactory(GsonConverterFactory.create())
                        .build();
                SimpleRetrofit simpleRetrofit = client.create(SimpleRetrofit.class);
                Call<DatabuffetItem> call = simpleRetrofit.getSearchData(value);
                call.enqueue(new Callback<DatabuffetItem>() {

                    @Override
                    public void onResponse(Response<DatabuffetItem> response) {
                        Log.d("", String.valueOf(response.raw()));

                        if (response.body() != null) {
                            Intent intent = new Intent(SearchActivity.this, DataRestaurantActivity.class);
                            Bundle bundle = new Bundle();
                            bundle.putInt("idrestauranFood1", response.body() .getDataBuffet().get(0).getIdrestauran());
                            bundle.putInt("idtypeFood1", response.body() .getDataBuffet().get(0).getIdtype());
                            bundle.putString("imgFood", response.body() .getDataBuffet().get(0).getImage());
                            bundle.putString("nameFood", response.body() .getDataBuffet().get(0).getName());
                            bundle.putString("addFood",response.body() .getDataBuffet().get(0).getAdd());
                            bundle.putString("telFood",response.body() .getDataBuffet().get(0).getTel());
                            bundle.putInt("minFood", response.body() .getDataBuffet().get(0).getPriceMin());
                            bundle.putInt("maxFood", response.body() .getDataBuffet().get(0).getPriceMax());
                            bundle.putString("mapFood",response.body() .getDataBuffet().get(0).getMap());
                            intent.putExtras(bundle);
                            startActivity(intent);

//                            startActivity(new Intent(SearchActivity.this, GeneralActivity.class));
                        }
                    }

                    @Override
                    public void onFailure(Throwable t) {

                    }
                });



            }
        });

    }

}
