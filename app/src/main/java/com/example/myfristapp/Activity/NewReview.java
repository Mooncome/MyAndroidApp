package com.example.myfristapp.Activity;

import android.Manifest;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.myfristapp.ApplicationConfig;
import com.example.myfristapp.Fragment.HomeFragment;
import com.example.myfristapp.R;
import com.example.myfristapp.Retrofit.NewReviewResItem;
import com.example.myfristapp.Retrofit.NewreviewItem;
import com.example.myfristapp.Retrofit.RegisterItem;
import com.example.myfristapp.Retrofit.SimpleRetrofit;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.kosalgeek.android.photoutil.CameraPhoto;
import com.kosalgeek.android.photoutil.GalleryPhoto;
import com.kosalgeek.android.photoutil.ImageLoader;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import retrofit.Call;
import retrofit.Callback;
import retrofit.GsonConverterFactory;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * Created by DELL on 7/20/2016.
 */
public class NewReview extends AppCompatActivity implements View.OnClickListener {
    private  final  String TAG = this.getClass().getName();

    private Button btn_submit;
    private ImageView img_view;
    private EditText et_name;
    private EditText et_data;
    private EditText et_add;
    private String image;
    private String name;
    private String data;
    private String add;
    private String comment;
    ImageView ivcamera,ivgallory;
    CameraPhoto cameraPhoto;
    GalleryPhoto galleryPhoto;

    final int CAMERA_REQUEST = 13323;
    final int GALLERY_REQUEST = 12232;

    String[] PERMISSIONS = { android.Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.CAMERA};
    int PERMISSION_ALL = 1;
    private int userId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_newactivity);

        btn_submit = (Button) findViewById(R.id.btn_submit);
        img_view = (ImageView) findViewById(R.id.imageView1);


        et_name = (EditText) findViewById(R.id.editText);
        et_data = (EditText) findViewById(R.id.editText2);


        btn_submit.setOnClickListener(this);
//        img_view.setOnClickListener(this);
        SharedPreferences sp = getSharedPreferences("User_ID", Context.MODE_PRIVATE);
        userId = sp.getInt("User_ID",-1);


        cameraPhoto = new CameraPhoto(getApplicationContext());
        galleryPhoto = new GalleryPhoto(getApplicationContext());

        ivcamera = (ImageView)findViewById(R.id.image_carmera);
        ivgallory = (ImageView)findViewById(R.id.image_gallory);

        ivcamera.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                if(!hasPermissions(NewReview.this, PERMISSIONS)){
                    ActivityCompat.requestPermissions(NewReview.this, PERMISSIONS, PERMISSION_ALL);
                }else {
                    try {
                        startActivityForResult(cameraPhoto.takePhotoIntent(), CAMERA_REQUEST);
                        cameraPhoto.addToGallery();
                    } catch (IOException e) {
                        Toast.makeText(getApplicationContext(),
                                "Somthing Wrong while taking photos", Toast.LENGTH_SHORT).show();

                    }
                }
                }




        });

        ivgallory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!hasPermissions(NewReview.this, PERMISSIONS)){
                    ActivityCompat.requestPermissions(NewReview.this, PERMISSIONS, PERMISSION_ALL);
                }else {
                    startActivityForResult(galleryPhoto.openGalleryIntent(), GALLERY_REQUEST);

                }

            }
        });


    }
    public static boolean hasPermissions(Context context, String... permissions) {
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }
    String photopath,imageName,imagePath;
    Bitmap bitmap;
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(resultCode == RESULT_OK){
            if(requestCode == CAMERA_REQUEST){
                 photopath = cameraPhoto.getPhotoPath();
                File f1 = new File(photopath);
                imageName = f1.getName();
                imagePath = f1.getPath();
                Log.d(TAG, photopath);
                try{
                     bitmap = ImageLoader.init().from(photopath).requestSize(400,450).getBitmap();
                    img_view.setImageBitmap(bitmap);
                } catch (FileNotFoundException e) {
                    Toast.makeText(getApplicationContext(),
                            "Somthing Wrong while loading photos", Toast.LENGTH_SHORT).show();

                }
            }
            else if (requestCode == GALLERY_REQUEST){
                Uri uri = data.getData();
                galleryPhoto.setPhotoUri(uri);
                 photopath = galleryPhoto.getPath();
                File f1 = new File(photopath);
                imageName = f1.getName();
                imagePath = f1.getPath();
                try{
                     bitmap = ImageLoader.init().from(photopath).requestSize(400,450).getBitmap();
                    img_view.setImageBitmap(bitmap);
                } catch (FileNotFoundException e) {
                    Toast.makeText(getApplicationContext(),
                            "Somthing Wrong while choosing photos", Toast.LENGTH_SHORT).show();

                }

            }
        }
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_submit:




                name = et_name.getText().toString();
                comment = et_data.getText().toString();


                uploadImageToFireBase(bitmap,imageName,imagePath,comment,name);
                Toast.makeText(NewReview.this, "ขอบคุณสำหรับการรีวิว รอการตรวจสอบ", Toast.LENGTH_LONG).show();
                startActivity(new Intent(this, HomeActivity.class));

                break;
        }


//
//        switch (v.getId()) {
//            case R.id.image_carmera:
//
//                try {
//                    startActivityForResult(cameraPhoto.takePhotoIntent(), CAMERA_REQUEST);
//                } catch (IOException e) {
//                    Toast.makeText(getApplicationContext(),
//                            "Somthing Wrong while taking photos", Toast.LENGTH_SHORT).show();
//                }
//
//
//        }

//        switch (v.getId()) {
//            case R.id.imageView:
//
//
//                startActivity(new Intent(this, CustomDialog.class));
//
////                final Dialog dialog = new Dialog(NewReview.this);
////                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
////                dialog.setContentView(R.layout.customdialog);
////                dialog.setCancelable(true);
////                dialog.show();
//
//
//
//
//
//                break;
//        }


    }

    public void uploadImageToFireBase(Bitmap bitmap, String nameImage, String path, final String comment, final String name) {


        FirebaseStorage storage = FirebaseStorage.getInstance();
        StorageReference storageRef = storage.getReferenceFromUrl("gs://myfrist-app.appspot.com");


        // Create a reference to "mountains.jpg"
        StorageReference imageRef = storageRef.child("" + System.currentTimeMillis() + nameImage);

        // Create a reference to 'images/mountains.jpg'
        StorageReference pathImagesRef = storageRef.child(path);

        // While the file names are the same, the references point to different files
        imageRef.getName().equals(pathImagesRef.getName());    // true
        imageRef.getPath().equals(pathImagesRef.getPath());    // false




        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] data = baos.toByteArray();

        UploadTask uploadTask = pathImagesRef.putBytes(data);

        uploadTask.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                // Handle unsuccessful uploads
            }
        }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                // taskSnapshot.getMetadata() contains file metadata such as size, content-type, and download URL.
                Uri imageUrl = taskSnapshot.getDownloadUrl();

                Retrofit client = new Retrofit.Builder()
                        .baseUrl(ApplicationConfig.URL)
                        .addConverterFactory(GsonConverterFactory.create())
                        .build();
                final SimpleRetrofit simpleRetrofit = client.create(SimpleRetrofit.class);
                Call<NewReviewResItem> call = simpleRetrofit.getReviewData(comment, name, String.valueOf(imageUrl),userId);
                call.enqueue(new Callback<NewReviewResItem>() {
                    @Override
                    public void onResponse(Response<NewReviewResItem> response) {

                        Log.d("dd", response.raw().toString());

                        if (response != null) {

                            if (response.body().getMessage().equals("สำเร็จ")) {
                                Toast.makeText(NewReview.this, "AddNewRestaurent Success", Toast.LENGTH_LONG).show();
                                startActivity(new Intent(NewReview.this, HomeActivity.class));

                            } else {
//                                Toast.makeText(NewReview.this, " Please try again", Toast.LENGTH_LONG).show();

                            }


                        }


                    }

                    @Override
                    public void onFailure(Throwable t) {
                        Toast.makeText(NewReview.this, "AddNewRestaurent Failed", Toast.LENGTH_LONG).show();

                    }
                });

            }
        });
    }



}
