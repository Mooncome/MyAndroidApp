package com.example.myfristapp.Fragment;


import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.myfristapp.Manifest;
import com.example.myfristapp.R;
import com.example.myfristapp.SingleShotLocationProvider;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import io.nlopez.smartlocation.OnLocationUpdatedListener;
import io.nlopez.smartlocation.SmartLocation;


/**
 * A simple {@link Fragment} subclass.
 */
public class NearbyFragment extends Fragment implements  OnMapReadyCallback{

    private GoogleMap googleMap;
    private FragmentManager supportFragmentManager;

    private OnLocationUpdatedListener onLocationUpdatedListener;
    private int MY_LOCATION_REQUEST_CODE;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // inflate and return the layout
        View v = inflater.inflate(R.layout.fragment_nearby, container,
                false);
//        mMapView = (MapView) v.findViewById(R.id.mapView);
//        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
//                .findFragmentById(R.id.mapView);
//        mapFragment.getMapAsync((OnMapReadyCallback) getMap());
//        mMapView.onCreate(savedInstanceState);
//
//        mMapView.onResume();// needed to get the map to display immediately
//
//        try {
//            MapsInitializer.initialize(getActivity().getApplicationContext());
//        } catch (Exception e) {
//            e.printStackTrace();
//        }



//        mMapView = ((MapFragment) getFragmentManager().findFragmentById(R.id.mapView)).getMap();
//        googleMap.addMarker(new MarkerOptions()
//                .position(new LatLng(18.449978, 98.674981))
//                .title(""));
////
//        mMapView = ((MapFragment) getFragmentManager().findFragmentById(R.id.mapView)).getMap();
//        googleMap.addMarker(new MarkerOptions()
//                .position(new LatLng(18.448397, 98.675269))
//                .title(""));
//
//        mMapView = ((MapFragment) getFragmentManager().findFragmentById(R.id.mapView)).getMap();
//        googleMap.addMarker(new MarkerOptions()
//                .position(new LatLng(18.434578, 98.682251))
//                .title(""));





//        googleMap = mMapView.getMap();
//         latitude and longitude
        double latitude = 18.469954;
        double longitude = 98.675485;



        // adding marker


//        .target(new LatLng(18.469954, 98.675485)).zoom(10).build();

        //ร้านอาหารทั่วไป




        SupportMapFragment mapFragment = (SupportMapFragment)this.getChildFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);







        // Perform any camera updates here
        return v;
    }

    private void position(LatLng latLng) {

    }


   /* private void setUpMapIfNeeded() {
        if (googleMap == null) {
            // Try to obtain the map from the SupportMapFragment.
            googleMap = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.mapView))
                    .getMap();
            // Check if we were successful in obtaining the map.
            if (googleMap != null) {


                googleMap.setOnMyLocationChangeListener(new GoogleMap.OnMyLocationChangeListener() {

                    @Override
                    public void onMyLocationChange(Location arg0) {
                        // TODO Auto-generated method stub

                        googleMap.addMarker(new MarkerOptions().position(new LatLng(arg0.getLatitude(), arg0.getLongitude())).title("It's Me!"));
                    }
                });

            }
        }



    }
*/
//    @Override
//    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
//        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
//                == PackageManager.PERMISSION_GRANTED) {
//            googleMap.setMyLocationEnabled(true);
//        } else {
//            // Show rationale and request permission.
//        }
//
//        if (requestCode == MY_LOCATION_REQUEST_CODE) {
//            if (permissions.length == 1 &&
//                    permissions[0] == Manifest.permission.ACCESS_FINE_LOCATION &&
//                    grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//                googleMap.setMyLocationEnabled(true);
//            } else {
//                // Permission was denied. Display an error message.
//            }
//        }
//    }


    @Override
    public void onResume() {
        super.onResume();
//        mMapView.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
//        mMapView.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
//        mMapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
//        mMapView.onLowMemory();
    }

    public FragmentManager getSupportFragmentManager() {
        return supportFragmentManager;
    }

//    public MapView getMap() {
////        return mMapView;
//    }

//    @Override
//    public void onLocationUpdated(Location location) {
//        SmartLocation.with(context)
//                .location()
//                .continuous()
//                .start(onLocationUpdatedListener);
//
//
//
//    }
//
//    @Override
//    public void onStop() {
//        SmartLocation.with(context)
//                .location()
//                .stop();
//        super.onStop();
//    }

    public void loadLocation(final GoogleMap googleMap){
        if (!checkPermission(getActivity())) {
            ActivityCompat.requestPermissions(
                    getActivity(),
                    new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                    0);
        } else {
            SingleShotLocationProvider.requestSingleUpdate(getActivity(),
                    new SingleShotLocationProvider.LocationCallback() {
                        @Override
                        public void onNewLocationAvailable(SingleShotLocationProvider.GPSCoordinates location) {
                            Log.d("Location", "my location is " + location.latitude+"."+location.longitude);

                              String lat = location.latitude+"";
                            String lng = location.longitude+"";


                            LatLng map_latlng = new LatLng(Double.parseDouble(lng), Double.parseDouble(lat));
                            googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(map_latlng, 11));
                            googleMap.addMarker(new MarkerOptions()
                                    .title("คุณ")
                                    .anchor(0.0f, 1.0f)
                                    .icon(BitmapDescriptorFactory
                                            .defaultMarker(BitmapDescriptorFactory.HUE_ROSE))// Anchors the marker on the bottom left
                                            .position(map_latlng));


//
//                            googleMap.addMarker(new MarkerOptions()
//                                    .position(new LatLng(18.434578, 98.682251))
//                                    .title("ร้านเอเอ หมูกะทะ"));
//
//                            googleMap.addMarker(new MarkerOptions()
//                                    .position(new LatLng(18.448397, 98.675269))
//                                    .title("ร้านลุงโบ้"));
//
//                            googleMap.addMarker(new MarkerOptions()
//                                    .position(new LatLng(18.449978, 98.674981))
//                                    .title("ร้านสองพี่น้อง"));
//
//                            googleMap.addMarker(new MarkerOptions()
//                                    .position(new LatLng(18.795194, 98.966794))
//                                    .title("Khunmor cuisine คุณหมอคูซีน"));
//
//                            googleMap.addMarker(new MarkerOptions()
//                                    .position(new LatLng(18.796711, 98.965980))
//                                    .title("Mu's Katsu"));
//
//                            googleMap.addMarker(new MarkerOptions()
//                                    .position(new LatLng(18.785005, 98.994426))
//                                    .title("Rock Me Burger&bar"));
//


                        }
                    });
        }
    }
    public static boolean checkPermission(final Context context) {
        return ActivityCompat.checkSelfPermission(context, android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
                ;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        loadLocation(googleMap);
        googleMap.addMarker(new MarkerOptions()
                .position(new LatLng(18.434578, 98.682251))
                .title("ร้านเอเอ หมูกะทะ"));

        googleMap.addMarker(new MarkerOptions()
                .position(new LatLng(18.448397, 98.675269))
                .title("ร้านลุงโบ้"));

        googleMap.addMarker(new MarkerOptions()
                .position(new LatLng(18.449978, 98.674981))
                .title("ร้านสองพี่น้อง"));

        googleMap.addMarker(new MarkerOptions()
                .position(new LatLng(18.795194, 98.966794))
                .title("Khunmor cuisine คุณหมอคูซีน"));

        googleMap.addMarker(new MarkerOptions()
                .position(new LatLng(18.796711, 98.965980))
                .title("Mu's Katsu"));

        googleMap.addMarker(new MarkerOptions()
                .position(new LatLng(18.785005, 98.994426))
                .title("Rock Me Burger&bar"));

        googleMap.addMarker(new MarkerOptions()
                .position(new LatLng(18.784428, 99.046009))
                .title("ร้านมีนา"));

        googleMap.addMarker(new MarkerOptions()
                .position(new LatLng(18.798076, 98.973862))
                .title("Aisushi Chiang Mai"));

        googleMap.addMarker(new MarkerOptions()
                .position(new LatLng(18.808833, 98.954126))
                .title("ร้านน้ำดอกไม้(Sweet Mango)"));

        googleMap.addMarker(new MarkerOptions()
                .position(new LatLng(18.800028, 98.975458))
                .title("เฮือนม่วนใจ๋"));

        googleMap.addMarker(new MarkerOptions()
                .position(new LatLng(18.725380, 99.102378))
                .title("เฮือนใจ๋ยอง"));

        googleMap.addMarker(new MarkerOptions()
                .position(new LatLng(18.737959, 98.933751))
                .title("เฮือนเพ็ญ"));

        //ร้านอาหารบุฟเฟ่

        googleMap.addMarker(new MarkerOptions()
                .position(new LatLng(18.762596, 99.006090))
                .title("GINZI สุกี้+จุ่มแซ่บ"));

        googleMap.addMarker(new MarkerOptions()
                .position(new LatLng(18.805997, 98.981497))
                .title("Retro Steak Cafe’"));

        googleMap.addMarker(new MarkerOptions()
                .position(new LatLng(18.752291, 99.009703))
                .title("Yamato Japanese Buffet"));

        googleMap.addMarker(new MarkerOptions()
                .position(new LatLng(18.7664247, 98.982856))
                .title("อันดามัน ซีฟู๊ด"));

        googleMap.addMarker(new MarkerOptions()
                .position(new LatLng(18.808325, 98.995145))
                .title("ดิบดี Sushi Café"));

        googleMap.addMarker(new MarkerOptions()
                .position(new LatLng(18.788886, 98.970540))
                .title("The Buffet"));

        googleMap.addMarker(new MarkerOptions()
                .position(new LatLng(18.856554, 99.099393))
                .title("Horizon Village Resort & Conference Centre"));

    }
}
