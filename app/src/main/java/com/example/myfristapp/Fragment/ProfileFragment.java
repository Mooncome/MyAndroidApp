package com.example.myfristapp.Fragment;


import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.myfristapp.Activity.HomeActivity;
import com.example.myfristapp.Activity.LoginActivity;
import com.example.myfristapp.ApplicationConfig;
import com.example.myfristapp.R;
import com.example.myfristapp.Retrofit.DatabuffetItem;
import com.example.myfristapp.Retrofit.EditProfileItem;
import com.example.myfristapp.Retrofit.MenuImageItem;
import com.example.myfristapp.Retrofit.ProfileItem;
import com.example.myfristapp.Retrofit.RegisterItem;
import com.example.myfristapp.Retrofit.SimpleRetrofit;

import retrofit.Call;
import retrofit.Callback;
import retrofit.GsonConverterFactory;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProfileFragment extends Fragment implements View.OnClickListener{

    EditText edt_name,edt_address,edt_email;
    Button btn_edit;
    String fristname, add,email;
    String profile_fristname,profile_add,profile_email;
    int userId;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_profile,container,false);

        edt_name = (EditText) view.findViewById(R.id.et_fristname);
        edt_address = (EditText) view.findViewById(R.id.et_add);
        edt_email = (EditText) view.findViewById(R.id.et_email);

        btn_edit = (Button) view.findViewById(R.id.btn_submit);
        btn_edit.setOnClickListener(this);


        SharedPreferences sp = getActivity().getSharedPreferences("User_ID", Context.MODE_PRIVATE);
        userId = sp.getInt("User_ID",-1);
        profile_fristname = sp.getString("First_Name", "");
        profile_add = sp.getString("Add","");
        profile_email = sp.getString("E_mail","");



        edt_name.setText(profile_fristname);
        edt_address.setText(profile_add);
        edt_email.setText(profile_email);
      /*  Retrofit client = new Retrofit.Builder()
                .baseUrl(ApplicationConfig.URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        SimpleRetrofit simpleRetrofit = client.create(SimpleRetrofit.class);
        Call<EditProfileItem> call = simpleRetrofit.getProfile(userId);
        call.enqueue(new Callback<EditProfileItem>() {
            @Override
            public void onResponse(Response<EditProfileItem> response) {
                Log.d("", String.valueOf(response.raw()));

                Log.d("", String.valueOf(response.body()));

                if (response.body() != null) {


                    edt_name.setText(response.body().getEditProfile().get(0).getFirstname());
                    edt_address.setText(response.body().getEditProfile().get(0).getAddress());
                    edt_email.setText(response.body().getEditProfile().get(0).getEmail());


                }
            }

            @Override
            public void onFailure(Throwable t) {

            }




        });

*/

        return view;

    }



    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_submit:

                fristname = edt_name.getText().toString();
                add = edt_address.getText().toString();
                email = edt_email.getText().toString();

                Retrofit client = new Retrofit.Builder()
                        .baseUrl(ApplicationConfig.URL)
                        .addConverterFactory(GsonConverterFactory.create())
                        .build();
                final SimpleRetrofit simpleRetrofit = client.create(SimpleRetrofit.class);
                Call<ProfileItem> call = simpleRetrofit.getProfileEdit(fristname, add, email,userId);
                call.enqueue(new Callback<ProfileItem>() {
                    @Override
                    public void onResponse(Response<ProfileItem> response) {

                        Log.d("dd", response.raw().toString());

                        if (response != null) {


                            if (response.body().getMessage().equals("สำเร็จ")) {
                                Toast.makeText(getActivity(), "Edit profile Success", Toast.LENGTH_LONG).show();
                                startActivity(new Intent(getActivity(), HomeActivity.class));


                                SharedPreferences sp = getActivity().getSharedPreferences("User_ID", Context.MODE_PRIVATE);
                                SharedPreferences.Editor editor = sp.edit();

                                editor.putString("First_Name", fristname);
                                editor.putString("Add",add);
                                editor.putString("E_mail", email);
                                editor.commit();

                                SharedPreferences sp1 = getActivity().getSharedPreferences("User_ID", Context.MODE_PRIVATE);
                                profile_fristname = sp1.getString("First_Name", "");
                                profile_add = sp1.getString("Add","");
                                profile_email = sp1.getString("E_mail","");

                                edt_name.setText(profile_fristname);
                                edt_address.setText(profile_add);
                                edt_email.setText(profile_email);


                            } else {
                                Toast.makeText(getActivity(), "Edit profile unsuccess. Please try again", Toast.LENGTH_LONG).show();

                            }


                        }


                    }

                    @Override
                    public void onFailure(Throwable t) {
                        Toast.makeText(getActivity(), "Register Failed", Toast.LENGTH_LONG).show();

                    }
                });



                startActivity(new Intent(getActivity(), HomeActivity.class));


                break;
        }
    }
}
