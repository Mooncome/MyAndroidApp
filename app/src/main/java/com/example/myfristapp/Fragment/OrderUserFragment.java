package com.example.myfristapp.Fragment;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.myfristapp.Adapter.ResAdapter;
import com.example.myfristapp.ApplicationConfig;
import com.example.myfristapp.R;
import com.example.myfristapp.Retrofit.OrderItem;
import com.example.myfristapp.Retrofit.SimpleRetrofit;

import java.util.ArrayList;
import java.util.List;

import retrofit.Call;
import retrofit.Callback;
import retrofit.GsonConverterFactory;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * Created by DELL on 8/29/2016.
 */
public class OrderUserFragment extends Fragment {

    private RecyclerView recyclerView;
    private ResAdapter resAdapter;
    private int photo;
    private Object supportFragmentManager;
    private String map;
    private int idrestauran;
    private int idtype;
    private String name;
    private String add;
    private String status;
    private int pricemin;
    private int userId;
    public String image;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.order_user_fragment, container, false);

        recyclerView = (RecyclerView) view.findViewById(R.id.recyclerview_order);


        SharedPreferences sp = getActivity().getSharedPreferences("User_ID", Context.MODE_PRIVATE);
        status = sp.getString("status", "");
        userId = sp.getInt("User_ID", -1);


        if (status.equals("USER")){
            getOderUser();


        }else {
            getOderRestaurant();

        }



        return view;
    }

    private void getOderUser(){
        Retrofit client = new Retrofit.Builder()
                .baseUrl(ApplicationConfig.URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        SimpleRetrofit simpleRetrofit = client.create(SimpleRetrofit.class);
        Call<OrderItem> call = simpleRetrofit.getOrderFoodUser(userId);
        call.enqueue(new Callback<OrderItem>() {
            @Override
            public void onResponse(Response<OrderItem> response) {
                Log.d("", String.valueOf(response.raw()));

                Log.d("", String.valueOf(response.body()));

                if (response.body() != null) {

                   List<String> listId = new ArrayList<String>();
                    List<String> name = new ArrayList<String>();
                    for (int i = 0;i<response.body().getOrderFood().size();i++){

                        if (!listId.toString().contains(String.valueOf(response.body().getOrderFood().get(i).getIdrestaurant()))){

                            listId.add(String.valueOf(response.body().getOrderFood().get(i).getIdrestaurant()));
                            name.add(String.valueOf(response.body().getOrderFood().get(i).getRestaurantname()));
                        }

                    }


                    initRecycleGridView(listId,name,"USER");

                }
            }

            @Override
            public void onFailure(Throwable t) {

            }

            private void initRecycleGridView(List<String> body,List<String> name,String status) {
                recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 1));

                OrderAdapter orderAdapter = new OrderAdapter(body,name, getActivity(),status);
                recyclerView.setAdapter(orderAdapter);
            }


        });


    }
    private void getOderRestaurant(){
        Retrofit client = new Retrofit.Builder()
                .baseUrl(ApplicationConfig.URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        SimpleRetrofit simpleRetrofit = client.create(SimpleRetrofit.class);
        Call<OrderItem> call = simpleRetrofit.getOrderFoodRes(5);
        call.enqueue(new Callback<OrderItem>() {
            @Override
            public void onResponse(Response<OrderItem> response) {
                Log.d("", String.valueOf(response.raw()));

                Log.d("", String.valueOf(response.body()));

                if (response.body() != null) {
                    List<String> listId = new ArrayList<String>();
                    List<String> name = new ArrayList<String>();
                    for (int i = 0;i<response.body().getOrderFood().size();i++){

                        if (!listId.toString().contains(String.valueOf(response.body().getOrderFood().get(i).getIduser()))){

                            listId.add(String.valueOf(response.body().getOrderFood().get(i).getIduser()));
                            name.add(String.valueOf(response.body().getOrderFood().get(i).getUsername()));
                        }

                    }


                    initRecycleGridView(listId,name,"RESTAURANT");



                }
            }

            @Override
            public void onFailure(Throwable t) {

            }

            private void initRecycleGridView(List<String> body,List<String> name,String status) {
                recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 1));

                OrderAdapter orderAdapter = new OrderAdapter(body,name, getActivity(),status);
                recyclerView.setAdapter(orderAdapter);
            }



        });


    }
}
