package com.example.myfristapp.Fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.myfristapp.Activity.DataRestaurantActivity;
import com.example.myfristapp.ItemClickListener;
import com.example.myfristapp.R;
import com.example.myfristapp.Retrofit.DatabuffetItem;
import com.example.myfristapp.Retrofit.DataresItem;
import com.squareup.picasso.Picasso;

/**
 * Created by DELL on 7/21/2016.
 */
public class GeneralguidAdapter extends RecyclerView.Adapter<GeneralguidAdapter.GenHolder>implements ItemClickListener{


    private Context context;
    private DataresItem data;

    public GeneralguidAdapter(DataresItem dataFood,Context context) {
        this.context = context;
        this.data = dataFood;
    }



    @Override
    public GeneralguidAdapter.GenHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemLayoutView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.buffet, null);

        final GenHolder viewHolder = new GenHolder(itemLayoutView,this,context);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(GeneralguidAdapter.GenHolder holder, int position) {
        holder.textView_name.setText(data.getDataRestaurant().get(position).getName());
//            holder.imageView.setImageResource(data.getDataRestaurant().get(position).getGetimage());
        // holder.imageView.setImageResource(data.getImageDatares().get(position).getName());

        Picasso.with(context)
                .load(data.getDataRestaurant().get(position).getImage())
                .error(R.drawable.delete)
                .into(holder.imageView);


    }





    @Override
    public int getItemCount() {

        return 4;

    }






    @Override
    public void onItemClick(View view, int position) {
        Intent intent = new Intent(context, DataRestaurantActivity.class);
        Bundle bundle = new Bundle();
        bundle.putInt("idrestauranFood1", data.getDataRestaurant().get(position).getIdrestauran());
        bundle.putInt("idtypeFood1", data.getDataRestaurant().get(position).getIdtype());
        bundle.putString("imgFood", data.getDataRestaurant().get(position).getImage());
        bundle.putString("nameFood", data.getDataRestaurant().get(position).getName());
        bundle.putString("addFood",data.getDataRestaurant().get(position).getAdd());
        bundle.putString("telFood",data.getDataRestaurant().get(position).getTel());
        bundle.putInt("minFood", data.getDataRestaurant().get(position).getPriceMin());
        bundle.putInt("maxFood", data.getDataRestaurant().get(position).getPriceMax());
        bundle.putString("mapFood",data.getDataRestaurant().get(position).getMap());
        intent.putExtras(bundle);
        context.startActivity(intent);
    }

    public class GenHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        public ImageView imageView;
        public ItemClickListener mitemClickListener;
        public TextView textView_name;
        public Context context;

        public GenHolder(View itemView,ItemClickListener itemClickListener,Context context) {
            super(itemView);
            this.context=context;

            imageView = (ImageView) itemView.findViewById(R.id.imgbuf);
            textView_name = (TextView) itemView.findViewById(R.id.textView_name);
            mitemClickListener = itemClickListener;
            itemView.setOnClickListener((View.OnClickListener) this);
        }

        @Override
        public void onClick(View v) {
            mitemClickListener.onItemClick(v,getPosition());
        }
    }
}