package com.example.myfristapp.Fragment;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.myfristapp.ItemClickListener;
import com.example.myfristapp.R;
import com.example.myfristapp.Retrofit.OrderItem;

/**
 * Created by DELL on 12/16/2016.
 */
public class OrderListadapter extends RecyclerView.Adapter<OrderListadapter.OrderHolder> implements ItemClickListener {

    private final Context context;
    private OrderItem order;

    public OrderListadapter(OrderItem dataFood, Context context) {
        this.context = context;
        this.order = dataFood;
    }
    @Override
    public OrderListadapter.OrderHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemLayoutView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.order, null);
        final OrderListadapter.OrderHolder viewHolder = new OrderListadapter.OrderHolder(itemLayoutView,this,context);
        return viewHolder;



    }

    @Override
    public void onBindViewHolder(OrderListadapter.OrderHolder holder, int position) {
        holder.textView_name1.setText(order.getOrderFood().get(position).getMenuname());
        holder.textView_name2.setText(order.getOrderFood().get(position).getQuantity());
//        if (!order.getOrderFood().get(position).getImage().equals("")) {
//            Picasso.with(context)
//                    .load(order.getOrderFood().get(position).getImage())
//                    .error(R.drawable.delete)
//                    .into(holder.imageView);
//        }

    }

    @Override
    public int getItemCount() {

        if (order.getOrderFood()==null) {
            return 0;
        }else {
            return  order.getOrderFood().size();
        }
    }

    @Override
    public void onItemClick(View view, int position) {

    }

    public class OrderHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        public ImageView imageView;
        public ItemClickListener mitemClickListener;
        public TextView textView_name1,textView_name2;
        public Context context;

        public OrderHolder(View itemView,ItemClickListener itemClickListener,Context context) {
            super(itemView);
            this.context=context;

            imageView = (ImageView) itemView.findViewById(R.id.order11);
            textView_name1 = (TextView) itemView.findViewById(R.id.textView_name2);
            textView_name2 = (TextView) itemView.findViewById(R.id.textView_name4);
            mitemClickListener = itemClickListener;
            itemView.setOnClickListener(this);



        }

        @Override
        public void onClick(View v) {
            mitemClickListener.onItemClick(v,getPosition());
        }
    }
}
