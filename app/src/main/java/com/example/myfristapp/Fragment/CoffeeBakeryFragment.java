package com.example.myfristapp.Fragment;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;

import com.example.myfristapp.Adapter.CoffeeBakeryAdapter;
import com.example.myfristapp.R;


/**
 * A simple {@link Fragment} subclass.
 */
public class CoffeeBakeryFragment extends Fragment {


    public static Fragment newInstance(Context context) {
        CoffeeBakeryFragment r = new CoffeeBakeryFragment();
        return r;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
        ViewGroup root = (ViewGroup) inflater.inflate(R.layout.fragment_coffee_bakery, null);
        GridView gv1=(GridView)root.findViewById(R.id.gridview);
        gv1.setAdapter(new CoffeeBakeryAdapter(getActivity()));
        return root;
    }



    public CoffeeBakeryFragment() {
        // Required empty public constructor
    }

}
