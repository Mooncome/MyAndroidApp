package com.example.myfristapp.Fragment;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.myfristapp.Adapter.RecyclerAdapter;
import com.example.myfristapp.ApplicationConfig;
import com.example.myfristapp.Item.VideoItem;
import com.example.myfristapp.R;
import com.example.myfristapp.Retrofit.SimpleRetrofit;

import retrofit.Call;
import retrofit.Callback;
import retrofit.GsonConverterFactory;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * A simple {@link Fragment} subclass.
 */
public class RecyclerFragment extends Fragment {

    private static final String TAG = "RecyclerFragment";
    private RecyclerAdapter recyclerAdapter;
    private RecyclerView recyclerView;

    public RecyclerFragment() {
        // Required empty public constructor
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootview = inflater.inflate(R.layout.recyclerlayout, container, false);
        recyclerView = (RecyclerView) rootview.findViewById(R.id.recycler_view);



        Retrofit client = new Retrofit.Builder()
                .baseUrl(ApplicationConfig.URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        SimpleRetrofit simpleRetrofit = client.create(SimpleRetrofit.class);
        Call<VideoItem> call = simpleRetrofit.getShot();
        call.enqueue(new Callback<VideoItem>() {
            @Override
            public void onResponse(Response<VideoItem> response) {
                Log.d("", String.valueOf(response.raw()));

                Log.d("", String.valueOf(response.body()));

                if (response.body() != null) {
                        initRecycleGridView(response.body());


                }
            }



            @Override
                public void onFailure(Throwable t) {
                Log.d("", String.valueOf(t));
            }
        });


        return rootview;




    }

    private void initRecycleGridView(VideoItem viedeoitem) {
        recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 2));

        RecyclerAdapter recycleradapter = new RecyclerAdapter(viedeoitem,getActivity());
        recyclerView.setAdapter(recycleradapter);
    }


}