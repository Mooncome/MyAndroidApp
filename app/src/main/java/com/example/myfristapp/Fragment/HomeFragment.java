package com.example.myfristapp.Fragment;


import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.PagerAdapter;
import android.support.v7.widget.GridLayoutManager;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.myfristapp.Activity.GeneralActivity;
import com.example.myfristapp.Adapter.BuffetAdapter;
import com.example.myfristapp.Adapter.GeneralAdapter;
import com.example.myfristapp.Adapter.HomeAdapter;
import com.example.myfristapp.Adapter.Home1Adapter;
import com.example.myfristapp.ApplicationConfig;
import com.example.myfristapp.HomeImageItem1;
import com.example.myfristapp.Item.HomeImageItem;
import com.example.myfristapp.R;
import com.example.myfristapp.Retrofit.DatabuffetItem;
import com.example.myfristapp.Retrofit.DataresItem;
import com.example.myfristapp.Retrofit.SimpleRetrofit;
import com.viewpagerindicator.CirclePageIndicator;

import java.util.ArrayList;

import cn.trinea.android.view.autoscrollviewpager.AutoScrollViewPager;
import retrofit.Call;
import retrofit.Callback;
import retrofit.GsonConverterFactory;
import retrofit.Response;
import retrofit.Retrofit;


/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends Fragment {


    private RecyclerView recyclerView1,recyclerView2;
    private AutoScrollViewPager viewPager;
    private CirclePageIndicator mCirclePageIndicator;
    private PagerAdapter PagerAdapter;

    private int photo;
    private Object supportFragmentManager;
    private String map;
    private int idrestauran;
    private int idtype;
    private String name;
    private String add;
    private String tel;
    private int pricemin;
    private int pricemax;
    public String image;
    private int userId;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home,container,false);
        viewPager = (AutoScrollViewPager) view.findViewById(R.id.Viewpager);
        recyclerView1 = (RecyclerView) view.findViewById(R.id.recycler_view1);
        recyclerView2 = (RecyclerView) view.findViewById(R.id.recycler_view2);

        SharedPreferences sp = getActivity().getSharedPreferences("User_ID", Context.MODE_PRIVATE);
        userId = sp.getInt("User_ID",-1);

        Retrofit client = new Retrofit.Builder()
                .baseUrl(ApplicationConfig.URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        SimpleRetrofit simpleRetrofit = client.create(SimpleRetrofit.class);
        Call<DatabuffetItem> call = simpleRetrofit.getRestaurantbuffet(map, idrestauran, idtype, name, add, tel, pricemin, pricemax, image);
        call.enqueue(new Callback<DatabuffetItem>() {
            @Override
            public void onResponse(Response<DatabuffetItem> response) {
                Log.d("", String.valueOf(response.raw()));

                Log.d("", String.valueOf(response.body()));

                if (response.body() != null) {
                    initRecycleGridView(response.body());


                }
            }

            private void initRecycleGridView(DatabuffetItem body) {
                recyclerView1.setLayoutManager(new GridLayoutManager(getActivity(), 2));

                BuffetguidAdapter bufAdapter = new BuffetguidAdapter(body, getActivity());
                recyclerView1.setAdapter(bufAdapter);
            }

            @Override
            public void onFailure(Throwable t) {

            }




        });


        client = new Retrofit.Builder()
                .baseUrl(ApplicationConfig.URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        simpleRetrofit = client.create(SimpleRetrofit.class);
        Call<DataresItem> callres = simpleRetrofit.getRestaurant();
        callres.enqueue(new Callback<DataresItem>() {
            @Override
            public void onResponse(Response<DataresItem> response) {
                Log.d("", String.valueOf(response.raw()));

                Log.d("", String.valueOf(response.body()));

                if (response.body() != null) {
                    initRecycleGridView(response.body());


                }
            }

            private void initRecycleGridView(DataresItem body) {
                recyclerView2.setLayoutManager(new GridLayoutManager(getActivity(), 2));

                GeneralguidAdapter genAdapter = new GeneralguidAdapter(body, getActivity());
                recyclerView2.setAdapter(genAdapter);
            }

            @Override
            public void onFailure(Throwable t) {

            }
        });

            return view;
    }





    public HomeFragment() {
        // Required empty public constructor
    }


    public int getPhoto() {

        return photo;
    }

    public Object getSupportFragmentManager() {

        return supportFragmentManager;
    }
}
