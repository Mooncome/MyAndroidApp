package com.example.myfristapp.Fragment;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.myfristapp.ItemClickListener;
import com.example.myfristapp.R;
import com.example.myfristapp.Retrofit.DataresItem;
import com.example.myfristapp.Retrofit.OrderItem;
import com.squareup.picasso.Picasso;

/**
 * Created by DELL on 12/16/2016.
 */
public class Orderadapter1 extends RecyclerView.Adapter<Orderadapter1.OrderHolder1> implements ItemClickListener {

    private final Context context;
    private OrderItem order;

    public Orderadapter1(OrderItem dataFood,Context context) {
        this.context = context;
        this.order = dataFood;
    }
    @Override
    public Orderadapter1.OrderHolder1 onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemLayoutView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.order1, null);
        final Orderadapter1.OrderHolder1 viewHolder1 = new Orderadapter1.OrderHolder1(itemLayoutView,this,context);
        return viewHolder1;



    }

    @Override
    public void onBindViewHolder(Orderadapter1.OrderHolder1 holder, int position) {
        holder.textView_name1.setText(order.getOrderFood().get(position).getMenuname());
//        holder.textView_name2.setText(order.getOrderFood().get(position).getQuantity());
//        if (!order.getOrderFood().get(position).getImage().equals("")) {
//            Picasso.with(context)
//                    .load(order.getOrderFood().get(position).getImage())
//                    .error(R.drawable.delete)
//                    .into(holder.imageView);
//        }

    }

    @Override
    public int getItemCount() {

        if (order.getOrderFood()==null) {
            return order.getOrderFood().size();
        }else {
            return 0;
        }
    }

    @Override
    public void onItemClick(View view, int position) {

    }

    public class OrderHolder1 extends RecyclerView.ViewHolder implements View.OnClickListener{
        public ImageView imageView;
        public ItemClickListener mitemClickListener;
        public TextView textView_name1,textView_name2;
        public Context context;

        public OrderHolder1(View itemView,ItemClickListener itemClickListener,Context context) {
            super(itemView);
            this.context=context;

            imageView = (ImageView) itemView.findViewById(R.id.order11);
            textView_name1 = (TextView) itemView.findViewById(R.id.textView_name2);
            textView_name2 = (TextView) itemView.findViewById(R.id.textView_name4);
            mitemClickListener = itemClickListener;
            itemView.setOnClickListener(this);



        }

        @Override
        public void onClick(View v) {
            mitemClickListener.onItemClick(v,getPosition());
        }
    }
}
