package com.example.myfristapp.Fragment;


import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.example.myfristapp.Activity.NewReview;
import com.example.myfristapp.Activity.SearchActivity;
import com.example.myfristapp.Adapter.BuffetAdapter;
import com.example.myfristapp.Adapter.Home1Adapter;
import com.example.myfristapp.ApplicationConfig;
import com.example.myfristapp.GuidAdapter;
import com.example.myfristapp.GuidImageItem;
import com.example.myfristapp.R;
import com.example.myfristapp.Retrofit.DatabuffetItem;
import com.example.myfristapp.Retrofit.ReviewItem;
import com.example.myfristapp.Retrofit.SimpleRetrofit;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import retrofit.Call;
import retrofit.Callback;
import retrofit.GsonConverterFactory;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * A simple {@link Fragment} subclass.
 */
public class MemberGuideFragment extends Fragment implements View.OnClickListener {

    private RecyclerView recyclerView1;
    private GuidAdapter guidadapter;
    private String comment;
    private String name;
    private int idreviwe;
    private String photo;
    private ImageView floatingActionButton;
    private int userId;

    public MemberGuideFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_member_guide,container,false);
        recyclerView1 = (RecyclerView) view.findViewById(R.id.recycler_view);
        floatingActionButton = (ImageView) view.findViewById(R.id.fab1);
        floatingActionButton.setOnClickListener((View.OnClickListener) this);

        SharedPreferences sp = getActivity().getSharedPreferences("User_ID", Context.MODE_PRIVATE);
        userId = sp.getInt("User_ID",-1);


        Retrofit client = new Retrofit.Builder()
                .baseUrl(ApplicationConfig.URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        SimpleRetrofit simpleRetrofit = client.create(SimpleRetrofit.class);
        Call<ReviewItem> call = simpleRetrofit.getReview();
        call.enqueue(new Callback<ReviewItem>() {
            @Override
            public void onResponse(Response<ReviewItem> response) {
                Log.d("", String.valueOf(response.raw()));

//                Log.d("", String.valueOf(response.body()));

                if (response.body() != null) {
                    initRecycleGridView(response.body());


                }
            }

            private void initRecycleGridView(ReviewItem body) {
                recyclerView1.setLayoutManager(new GridLayoutManager(getActivity(), 1));

                GuidAdapter guidadapter = new GuidAdapter(body, getActivity());
                recyclerView1.setAdapter(guidadapter);
            }

            @Override
            public void onFailure(Throwable t) {

            }

        });
            return view;


    }




    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.fab1:

                Log.d("dd", "test");
                startActivity(new Intent(getActivity(), NewReview.class));


                break;
        }
    }
}
