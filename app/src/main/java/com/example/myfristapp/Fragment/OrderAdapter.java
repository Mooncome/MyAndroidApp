package com.example.myfristapp.Fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.myfristapp.Activity.MenuActivity;
import com.example.myfristapp.Activity.OrderListActivity;
import com.example.myfristapp.ItemClickListener;
import com.example.myfristapp.R;
import com.example.myfristapp.Retrofit.OrderItem;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by DELL on 12/18/2016.
 */
public class OrderAdapter extends RecyclerView.Adapter<OrderAdapter.OrderHolder> implements ItemClickListener {

    private final Context context;
    private List<String> listId;
    private String status;
    private List<String> name;

    public OrderAdapter(List<String> listId,List<String> name, Context context,String status) {
        this.context = context;
        this.listId = listId;
        this.name = name;
        this.status = status;
    }
    @Override
    public OrderAdapter.OrderHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemLayoutView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list, null);
        final OrderAdapter.OrderHolder viewHolder = new OrderAdapter.OrderHolder(itemLayoutView,this,context);
        return viewHolder;



    }

    @Override
    public void onBindViewHolder(OrderAdapter.OrderHolder holder, int position) {
        if (status.equals("USER")){
            holder.textView_name2.setText("Restaurant name :");


        }else{
            holder.textView_name2.setText("User name :");
        }
        holder.textView_name1.setText(name.get(position).toString());



    }

    @Override
    public int getItemCount() {

        if (listId==null) {
            return 0;
        }else {
            return  listId.size();
        }
    }

    @Override
    public void onItemClick(View view, int position) {

        Intent intent = new Intent(context, OrderListActivity.class);
        Bundle bundle = new Bundle();
        bundle.putInt("id", Integer.parseInt(listId.get(position).toString()));
        bundle.putString("status", status);
        intent.putExtras(bundle);
        context.startActivity(intent);

    }

    public class OrderHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        public ImageView imageView;
        public ItemClickListener mitemClickListener;
        public TextView textView_name1,textView_name2;
        public Context context;

        public OrderHolder(View itemView,ItemClickListener itemClickListener,Context context) {
            super(itemView);
            this.context=context;


            textView_name1 = (TextView) itemView.findViewById(R.id.textView_name2);
            textView_name2 = (TextView) itemView.findViewById(R.id.textView_name1);

            mitemClickListener = itemClickListener;
            itemView.setOnClickListener(this);



        }

        @Override
        public void onClick(View v) {
            mitemClickListener.onItemClick(v,getPosition());
        }
    }
}


