package com.example.myfristapp.Fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.myfristapp.R;
import com.example.myfristapp.Adapter.ResAdapter;
import com.example.myfristapp.Retrofit.ResImageItem;

import java.util.ArrayList;


public class RestaurantFragment extends Fragment {

    private RecyclerView recyclerView;
    private ResAdapter resAdapter;



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_restaurant,container,false);
        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);



        ArrayList<ResImageItem> resList = new ArrayList<ResImageItem>();
        resList.add(new ResImageItem(R.drawable.imgbuf,"buffet"));
        resList.add(new ResImageItem(R.drawable.imggel,"general"));

        recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 1));
        resAdapter =  new ResAdapter(resList,getActivity());
        recyclerView.setAdapter(resAdapter);





        return view;
    }




    public RestaurantFragment() {

    }




}
