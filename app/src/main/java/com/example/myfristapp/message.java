package com.example.myfristapp;

import com.example.myfristapp.Item.DataList1;
import com.example.myfristapp.Retrofit.DataList;
import com.google.gson.annotations.SerializedName;

/**
 * Created by DELL on 4/19/2016.
 */
public class message {
    private String status;
    private DataList data;


    public String getStatus() {
        return status;
    }

    public DataList getDatalist() {
        return data;
    }

    @SerializedName("Data")
    private DataList1 datalist;

}
