package com.example.myfristapp;


import android.view.View;

/**
 * Created by User on 24/11/2558.
 */
public interface ItemClickListener {
    void onItemClick(View view, int position);
}
