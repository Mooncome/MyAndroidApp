package com.example.myfristapp;

/**
 * Created by DELL on 7/20/2016.
 */
public class GuidImageItem {
    public int imgeView;
    public String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public GuidImageItem(int imgeView, String name) {
        this.imgeView = imgeView;
        this.name = name;

    }


    public int getImgeView() {

        return imgeView;
    }
}
