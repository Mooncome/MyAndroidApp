package com.example.myfristapp.Retrofit;

/**
 * Created by DELL on 7/21/2016.
 */
public class Imageeview {
    private int idrestaurant;
    private int idype;
    private String menuname;
    private String menuameimage;

    public int getIdrestaurant() {
        return idrestaurant;
    }

    public int getIdype() {
        return idype;
    }

    public String getMenuname() {
        return menuname;
    }

    public String getMenuameimage() {
        return menuameimage;
    }
}
