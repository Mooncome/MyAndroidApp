package com.example.myfristapp.Retrofit;

/**
 * Created by DELL on 12/2/2016.
 */
public class ProfileEdit {
    private String firstname;
    private String address;
    private String Email;
    private int user_id;

    public String getFirstname() {
        return firstname;
    }

    public String getAddress() {
        return address;
    }

    public String getEmail() {
        return Email;
    }

    public int getUserId() {
        return user_id;
    }
}
