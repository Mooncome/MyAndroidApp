package com.example.myfristapp.Retrofit;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by DELL on 12/2/2016.
 */
public class ProfileItem {
    public String message;

    public String getMessage() {
        return message;
    }

    public ArrayList<ProfileEdit> getProfileEdit() {
        return profile;
    }

    @SerializedName("data")
    private ArrayList<ProfileEdit> profile;
}
