package com.example.myfristapp.Retrofit;

import com.example.myfristapp.Datalogin.Loginstatus;
import com.example.myfristapp.Item.VideoItem;

import retrofit.Call;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.Query;

public interface SimpleRetrofit {
    @GET("/imhungry/cmumobile/api/Cmuonline/getCuteajarnBytype?cuteId=1&mode=1&nStart=0&pStart=0")
    Call<VideoItem>  getShot();

    @POST("/imhungry/login")
    Call<Loginstatus> getLogin(@Query("username") String username, @Query("password") String password);

    @POST("/imhungry/register")
    Call<RegisterItem> getRegister(@Query("firstname")String fristname,@Query("add")String add,@Query("email") String email,@Query("username") String username,@Query("password") String password,@Query("status")String status);

    @GET("/imhungry/restaurant")
    Call<DataresItem>  getRestaurant();

//    @GET("/restaurantimage")
//    Call<ImageDataItem> getRestaurantimage(@Query("idrestaurantimage")int idrestaurantimage,@Query("idrestaurant")int idrestaurant,@Query("name")String name);

    @GET("/imhungry/restaurantbuffet")
    Call<DatabuffetItem> getRestaurantbuffet(@Query("map")String map,@Query("idrestauran")int idrestauran,@Query("idtype")int idtype,@Query("name")String name,@Query("add")String add,@Query("tel")String tel,@Query("pricemin")int pricemin,@Query("pricemax")int pricemax,@Query("image")String image);

    @POST("/imhungry/menu")
    Call<MenuImageItem> getMenuImageItem();

    @GET("/imhungry/menu")
    Call<MenuFoodItem> getDataMenuFood();


    @GET("/imhungry/menubuffet")
    Call<MenubuffetImageItem> getMenubuffetImageItem(@Query("idmenu")int idmenu,@Query("idrestaurant")int idrestaurant,@Query("name")String name,@Query("price")String price,@Query("idtype")int idtype,@Query("photo")String photo);

//    @POST("/review")
//    Call<NewreviewItem> getNewreview(@Query("name")String name,@Query("comment")String data);

    @POST("/imhungry/apiImageMenu")
    Call<ImageItem> getImageview(@Query("drestaurant")int drestaurant,@Query("idtype")int idtype);

    @GET("/imhungry/review")
    Call<ReviewItem> getReview();

    @POST("/imhungry/apiMenuOrder")
    Call<MenuOrderItem> getOrderdata(@Query("idmenuorder")int idmenuorder,@Query("idrestaurant")int idrestaurant,@Query("menuname")String menuname,@Query("quantity")int quantity,@Query("iduser")int iduser,@Query("username")String username,@Query("restaurantname")String restaurantname);

    @POST("/imhungry/apiReview")
    Call<NewReviewResItem> getReviewData(@Query("comment")String comment,@Query("name")String name,@Query("imagereviwe")String imagereviwe,@Query("userId")int userId);

    @GET("/imhungry/profile")
    Call<EditProfileItem>getProfile(@Query("user_id")int userId);

//    @POST("/profile")
//    Call<EditProfileItem1>getEditProfile1(@Query("userid")int userId);


    @POST("/imhungry/apiprofile")
    Call<ProfileItem>getProfileEdit(@Query("firstname")String firstname,@Query("address")String address,@Query("Email")String Email,@Query("userId")int userId);


    @POST("/imhungry/order")
    Call<OrderItem> getOrderFoodUser(@Query("user_id")int user_id);
    @POST("/imhungry/order")
    Call<OrderItem> getOrderFoodRes(@Query("restaurant_id")int restaurant_id);
    @POST("/imhungry/order")
    Call<OrderItem> getOrderFoodWithId(@Query("user_id")int user_id,@Query("restaurant_id")int restaurant_id);

    @POST("/imhungry/send_id")
    Call<DatabuffetItem>pushSendId(@Query("user_id")String user_id,@Query("token")String token);
    @POST("/imhungry/confirm")
    Call<DatabuffetItem>pushconfirm(@Query("user_id")String user_id,@Query("token")String token);
    @POST("/imhungry/cancel")
    Call<DatabuffetItem>pushcancel(@Query("user_id")String user_id,@Query("token")String token);
    @POST("/imhungry/search")
    Call<DatabuffetItem>getSearchData(@Query("restaurantname")String restaurantname);

}
