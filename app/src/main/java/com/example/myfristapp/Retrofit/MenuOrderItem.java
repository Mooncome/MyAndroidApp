package com.example.myfristapp.Retrofit;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by DELL on 11/23/2016.
 */
public class MenuOrderItem {

    public String message;

    public String getMessage() {
        return message;
    }

    public ArrayList<OrderData> getOrderdata() {
        return orderdata;
    }

    @SerializedName("data")
    private ArrayList<OrderData> orderdata;

}
