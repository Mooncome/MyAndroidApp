package com.example.myfristapp.Retrofit;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by DELL on 12/7/2016.
 */
public class EditProfileItem1 {
    public String message;

    public String getMessage() {
        return message;
    }

    public ArrayList<EditProfile1> getEditProfile1() {
        return editprofile1;
    }

    @SerializedName("data")
    private ArrayList<EditProfile1> editprofile1;

}
