package com.example.myfristapp.Retrofit;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by DELL on 12/2/2016.
 */
public class EditProfileItem {
    public String message;

    public String getMessage() {
        return message;
    }

    public ArrayList<EditProfile> getEditProfile() {
        return editprofile;
    }

    @SerializedName("data")
    private ArrayList<EditProfile> editprofile;

}
