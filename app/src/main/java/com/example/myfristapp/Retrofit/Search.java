package com.example.myfristapp.Retrofit;

/**
 * Created by DELL on 12/17/2016.
 */
public class Search extends SearchItem {
    private String restaurantname;
    private int restaurant_id;

    public String getRestaurantname() {
        return restaurantname;
    }

    public int getRestaurant_id() {
        return restaurant_id;
    }
}
