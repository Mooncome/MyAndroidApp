package com.example.myfristapp.Retrofit;

/**
 * Created by DELL on 7/21/2016.
 */
public class ImageDatares {
    private int idrestaurantimage;
    private int idrestaurant;
    private String name;


    public int getIdrestaurantimage() {
        return idrestaurantimage;
    }

    public int getIdrestaurant() {
        return idrestaurant;
    }

    public String getName() {
        return name;
    }
}
