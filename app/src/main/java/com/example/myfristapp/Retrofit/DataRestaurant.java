package com.example.myfristapp.Retrofit;

/**
 * Created by DELL on 7/20/2016.
 */
public class DataRestaurant {
    private String map;
    private int idrestauran;
    private int idtype;
    private String name;
    private String add;
    private String tel;
    private int priceMin;
    private int priceMax;
    public String image;

    public int getPriceMin() {
        return priceMin;
    }

    public int getPriceMax() {
        return priceMax;
    }

    public String getImage() {
        return image;
    }



    public String getMap() {
        return map;
    }

    public int getIdrestauran() {
        return idrestauran;
    }

    public int getIdtype() {
        return idtype;
    }

    public String getName() {
        return name;
    }

    public String getAdd() {
        return add;
    }

    public String getTel() {
        return tel;
    }




}
