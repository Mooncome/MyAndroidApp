package com.example.myfristapp.Retrofit;

/**
 * Created by DELL on 7/12/2016.
 */
public class ResImageItem {
        public int imgeView;
    public String type;

    public ResImageItem(int imgeView, String type) {
        this.imgeView = imgeView;
        this.type = type;
    }

    public ResImageItem(int imgeView) {
            this.imgeView = imgeView;

        }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setImgeView(int imgeView) {
        this.imgeView = imgeView;
    }

    public int getImgeView() {
            return imgeView;
        }


    }
