package com.example.myfristapp.Retrofit;

/**
 * Created by DELL on 12/2/2016.
 */
public class EditProfile {
    private String firstname;
    private String address;
    private String email;
    private int userid;

    public int getUserid() {
        return userid;
    }

    public String getFirstname() {
        return firstname;
    }

    public String getAddress() {
        return address;
    }

    public String getEmail() {
        return email;
    }
}
