package com.example.myfristapp.Retrofit;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by DELL on 12/17/2016.
 */
public class SearchItem {
    public String message;

    public String getMessage() {
        return message;
    }

    public ArrayList<Search> getSearchData() {
        return search;
    }

    @SerializedName("data")
    private ArrayList<Search> search;


}
