package com.example.myfristapp.Retrofit;

/**
 * Created by DELL on 7/21/2016.
 */
public class DataMenubuffet {

    private String idmenu;
    private int idrestaurant;
    private int name;
    private String price;
    private String idtype;
    private String photo;
    private int image;

    public String getIdmenu() {
        return idmenu;
    }

    public int getIdrestaurant() {
        return idrestaurant;
    }

    public int getName() {
        return name;
    }

    public String getPrice() {
        return price;
    }

    public String getIdtype() {
        return idtype;
    }

    public String getPhoto() {
        return photo;
    }

    public int getImage() {
        return image;
    }
}


