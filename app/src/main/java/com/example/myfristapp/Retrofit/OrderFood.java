package com.example.myfristapp.Retrofit;

/**
 * Created by DELL on 12/16/2016.
 */
public class OrderFood {
    private int iduser;
    private int idorder;
    private int idrestaurant;
    private String menuname;
    private String quantity;
    private String status;
    private String restaurantname;
    private String username;

    public String getUsername() {
        return username;
    }

    public String getRestaurantname() {
        return restaurantname;
    }

    public int getIdorder() {
        return idorder;
    }

    public int getIdrestaurant() {
        return idrestaurant;
    }

    public String getMenuname() {
        return menuname;
    }

    public int getIduser() {
        return iduser;
    }

    public String getQuantity() {
        return quantity;
    }

    public String getStatus() {
        return status;
    }
}
