package com.example.myfristapp.Retrofit;

/**
 * Created by DELL on 11/23/2016.
 */
public class OrderData {
    private int idmenuorder;
    private int idrestaurant;
    private String menuname;
    private int quantity;
    private  String username;
    private String restaurantname;




    public int getIdmenuorder() {
        return idmenuorder;
    }

    public OrderData(int idmenuorder, int idrestaurant, String menuname, int quantity,String restaurantname) {
        this.idmenuorder = idmenuorder;
        this.idrestaurant = idrestaurant;
        this.quantity = quantity;
        this.menuname = menuname;
        this.restaurantname = restaurantname;

    }



    public String getRestaurantname() {
        return restaurantname;
    }

    public int getIdrestaurant() {
        return idrestaurant;
    }

    public String getMenuname() {
        return menuname;
    }

    public int getQuantity() {
        return quantity;
    }

    public boolean equals(Object o) {
        if (!(o instanceof OrderData)) {
            return false;
        }
        OrderData other = (OrderData) o;
        return  idmenuorder == other.idmenuorder && idrestaurant==other.idrestaurant && menuname.equals(other.menuname) && quantity == other.quantity&& restaurantname.equals(other.restaurantname);
    }

    public int hashCode() {
        return menuname.hashCode();
    }
}
