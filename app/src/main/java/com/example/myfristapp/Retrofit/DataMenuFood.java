package com.example.myfristapp.Retrofit;

/**
 * Created by DELL on 11/18/2016.
 */
public class DataMenuFood {
    private int idmenu;
    private int idrestaurant;
    private String name;
    private  int price;
    private int idtype;
    public String photo;

    private String restaurantname;


    public String getRestaurantname() {
        return restaurantname;
    }

    public int getIdmenu() {
        return idmenu;
    }

    public int getIdrestaurant() {
        return idrestaurant;
    }

    public String getName() {
        return name;
    }

    public int getPrice() {
        return price;
    }

    public int getIdtype() {
        return idtype;
    }

    public String getPhoto() {
        return photo;
    }
}
