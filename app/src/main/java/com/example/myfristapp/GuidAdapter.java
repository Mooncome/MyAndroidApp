package com.example.myfristapp;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.myfristapp.Activity.BuffetActivity;
import com.example.myfristapp.Activity.DataRestaurantActivity;
import com.example.myfristapp.Adapter.Home1Adapter;
import com.example.myfristapp.Item.DataresImageItem;
import com.example.myfristapp.Retrofit.ReviewItem;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by DELL on 7/20/2016.
 */
public class GuidAdapter extends RecyclerView.Adapter<GuidAdapter.GuidHolder> implements ItemClickListener {

    private Context context;
    private ReviewItem guidFood;


    public GuidAdapter(ReviewItem  dataFood,Context context) {
        this.context = context;
        this.guidFood = dataFood;

    }




    @Override
    public GuidAdapter.GuidHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemLayoutView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.guid, null);
//
        final GuidAdapter.GuidHolder viewHolder = new GuidAdapter.GuidHolder(itemLayoutView,this,context);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(GuidHolder holder, int position) {
        holder.textView_name1.setText(guidFood.getReviewData().get(position).getName());
//            holder.imageView.setImageResource(data.getDataRestaurant().get(position).getGetimage());
        // holder.imageView.setImageResource(data.getImageDatares().get(position).getName());
        holder.textView_name2.setText(guidFood.getReviewData().get(position).getComment());
        Picasso.with(context)
                .load(guidFood.getReviewData().get(position).getImagereview())
                .error(R.drawable.delete)
                .into(holder.imageView);

    }

    @Override
    public int getItemCount() {
        return guidFood.getReviewData().size();
    }

    @Override
    public void onItemClick(View view, int position) {
//
//        Intent intent = new Intent(context, DataRestaurantActivity.class);
//        Bundle bundle = new Bundle();
//        bundle.putInt("imgFood", guidFood.get(position).getImgeView());
//        bundle.putString("nameFood", guidFood.get(position).getName());
//        intent.putExtras(bundle);
//        context.startActivity(intent);

    }

    public class GuidHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
public TextView textView_name1,textView_name2;
        public ImageView imageView;
        public ItemClickListener mitemClickListener;
        public TextView textView_name;
        public Context context;

        public GuidHolder(View itemView,ItemClickListener itemClickListener,Context context) {
            super(itemView);
            this.context=context;

            imageView = (ImageView) itemView.findViewById(R.id.imgguid);
            textView_name1 = (TextView) itemView.findViewById(R.id.textView_name_guid);
            textView_name2 = (TextView) itemView.findViewById(R.id.textView_name_guid2);
            mitemClickListener = itemClickListener;
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            mitemClickListener.onItemClick(v,getPosition());
        }
    }
}
